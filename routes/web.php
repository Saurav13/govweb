<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}', function ($locale){
    $locale = $locale == 'ne' ? 'ne' : 'en';
    Session::put('locale', $locale);
    return redirect()->back();
});

Route::get('/albums', 'HomeController@albums');
Route::get('/albums/{slug}', 'HomeController@albumImages');

Route::get('/notices', 'HomeController@notices');

Route::get('/news','HomeController@news');
Route::get('/news/{slug}','HomeController@singleNews');


Route::get('/reports', 'HomeController@reports');
Route::get('/reports/filter', 'HomeController@filterReports');
Route::get('/report/print', 'HomeController@printReport');

Route::get('/projects','HomeController@projects');
Route::get('/projects/{slug}','HomeController@singleProject');

Route::get('contact-us','HomeController@contact_us')->name('contact');
Route::post('contact-us','HomeController@contactSend')->name('contactSend');

Route::get('/citizen-charter','HomeController@citizenCharter');

Route::get('/','HomeController@index')->name('home');

// Route::get('/about', function () {
//     return view('frontend.about');
// });

Route::prefix('admin')->group(function(){
    Route::get('/login','Admin\Auth\LoginController@showLoginForm')->name('admin_login');
	Route::post('/login','Admin\Auth\LoginController@login');
	Route::post('/logout','Admin\Auth\LoginController@logout')->name('admin_logout');
	Route::post('/password/email','Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin_password.email');
	Route::post('/password/reset','Admin\Auth\ResetPasswordController@reset');
	Route::get('/password/reset','Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin_password.request');
	Route::get('/password/reset/{token}','Admin\Auth\ResetPasswordController@showResetForm')->name('admin_password.reset');
    
    Route::get('profile','Admin\ProfileController@index')->name('profile');
    Route::post('profile/changePassword','Admin\ProfileController@changePassword')->name('profile.changePassword');
    Route::post('profile/changeName','Admin\ProfileController@updateName')->name('profile.updateName');

    Route::resource('infos', 'Admin\InfoController',['except' => ['create']]);
    Route::resource('projects', 'Admin\ProjectController',['except' => ['create']]);
    Route::resource('notices', 'Admin\NoticeController',['except' => ['create']]);
    Route::resource('news', 'Admin\NewsController',['except' => ['create']]);
    Route::resource('reports', 'Admin\ReportController',['except' => ['create']]);
    Route::resource('personnels', 'Admin\PersonnelController',['except' => ['create']]);
    Route::resource('citizencharters', 'Admin\CitizenCharterController',['except' => ['create']]);

    Route::post('albums/{id}/addImages','Admin\AlbumController@addImages')->name('albums.addImages');
    Route::delete('albums/{album_id}/deleteImage/{image_id}','Admin\AlbumController@deleteImage')->name('albums.deleteImage');
    Route::post('gallery/editcaption','Admin\AlbumController@editCaption');
    Route::resource('albums', 'Admin\AlbumController',['except' => ['create','edit']]);

    Route::get('settings','Admin\SettingsController@index')->name('admin.settings');
    Route::post('settings/addImage','Admin\SettingsController@addImage')->name('admin.settings.addImage');
    Route::delete('settings/removeImage/{id}','Admin\SettingsController@removeImage')->name('admin.settings.removeImage');
    Route::post('settings/contactUpdate','Admin\SettingsController@contactUpdate')->name('admin.settings.contactUpdate');
    Route::post('settings/aboutUpdate','Admin\SettingsController@aboutUpdate')->name('admin.settings.aboutUpdate');
    
    Route::get('newsletter','Admin\NewsletterController@newsletter')->name('newsletter');
    Route::post('newsletter/send','Admin\NewsletterController@newsletterSend')->name('newsletterSend');

    Route::get('contact-us-messages/unseen','Admin\ContactUsMessageController@unseenMsg')->name('contact-us-messages.unseen');
    Route::resource('contact-us-messages', 'Admin\ContactUsMessageController',['only' => ['show','destroy','index']]);
    Route::get('getUnseenMsgCount','Admin\ContactUsMessageController@getUnseenMsgCount')->name('getUnseenMsgCount');
    Route::get('getUnseenMsg','Admin\ContactUsMessageController@getUnseenMsg')->name('getUnseenMsg');

    // Route::resource('testimonials', 'Admin\TestimonialController',['except' => ['create','show']]);

    Route::get('dashboard','Admin\AdminController@index')->name('admin_dashboard');

    Route::get('/','Admin\AdminController@index');
});

Route::get('/asset/{source}/{img}/{h}/{w}',function($source,$img, $h, $w){
    $ext = explode(".",$img);
    $ext = end($ext);
    $source = str_replace('*','/',$source);
    return \Image::make(public_path("/".$source."/".$img))->resize($h, $w)->response($ext);
})->name('optimize');

Route::get('{alias}', 'HomeController@info');
