@extends('layouts.app')

@section('title',trans('labels.nav.projects'))

@section('body')

    <section class="g-my-40">
        <div class="container">
            <div class="text-center">
                <h4 class="g-mb-20">Latest Projects</h4>
            </div>
            <div class="row">
                @foreach ($projects as $project)
                    <div class="col-lg-4 g-mb-30">
                        <!-- Article -->
                        <article class="g-bg-white">
                            <figure class="g-pos-rel">
                                <img class="img-fluid w-100" src="{{ route('optimize', ['project_images',$project->image,350,300]) }}" alt="{{ $project->title }}">
                              
                            </figure>
        
                            <div class="g-pa-30">
                                <h3 class="h5 g-mb-15">
                                    <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/projects/{{ $project->slug }}">{{ $project->title }}</a>
                                </h3>
        
                                <p>
                                    {{ (str_limit(strip_tags($project->description), $limit = 170, $end = '...')) }}
                                </p>
                            </div>
                        </article>
                        <!-- End Article -->
                    </div>
                @endforeach
                
            </div>
            <nav class="text-center" aria-label="Page Navigation">
                {!! $projects->links('frontend.partials.paginate') !!}  
            </nav>

        </div>
    </section>
@endsection