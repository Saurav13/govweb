@extends('layouts.app')

@section('title',trans('labels.nav.citizenCharter'))

@section('body')
<section class="g-my-40">
    <div class="container">
        <div class="text-center">
            <h4 class="g-mb-20">@lang('labels.nav.citizenCharter')</h4>
        </div>
        <div class="g-mb-60">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Service Title</th>
                            <th>Service Fee</th>
                            <th>Service Time</th>
                            <th>Department</th>
                            <th>Remarks</th>

                        </tr>
                        </thead>
                    
                        <tbody>
                            @foreach($charters as $charter)
                        <tr>
                            <td>{{$charter->service_title}}</td>
                            <td>{{$charter->service_fee}}</td>
                            <td>{{$charter->service_time}}</td>
                            <td>{{$charter->responsible_dept}}</td>
                            <td>{{$charter->remarks}}</td>

                            
                        </tr>
                        @endforeach
                        
                        </tbody>
                    </table>
                    </div>
        </div>
    
    </div>
</section>

@endsection