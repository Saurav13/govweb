@extends('layouts.app')

@section('title',trans('labels.nav.notices'))

@section('body')
<section class="g-my-40">
    <div class="container">
        <div class="text-center">
            <h4 class="g-mb-20">Notices</h4>
        </div>
        <ul class="list-unstyled g-mb-60">
            @foreach ($notices as $notice)
                <!-- Events Item -->
                <li class="u-block-hover u-shadow-v37--hover {{ $loop->iteration % 2 == 1 ? 'g-bg-gray-light-v5' : 'g-bg-gray-light-v4' }} g-bg-white--hover rounded g-px-50 g-py-30 mb-4">
                    <div class="row align-items-lg-center">
                        <div class="col-md-3 col-lg-2 g-mb-30 g-mb-0--lg">
                            <div class="d-flex align-items-center mb-3">
                            <span class="g-color-primary g-font-size-50 g-line-height-1 mr-3">{{ date('j',strtotime($notice->published_date)) }}</span>
                            <div class="g-color-gray-dark-v4 text-center g-line-height-1_4">
                                <span class="d-block">{{ date('M',strtotime($notice->published_date)) }}</span>
                                <span class="d-block">{{ date('Y',strtotime($notice->published_date)) }}</span>
                            </div>
                            </div>
                            {{-- <span class="d-block g-color-gray-dark-v4">6.00pm - 7.15pm</span> --}}
                        </div>
                        <div class="col-md-9 col-lg-8 g-mb-30 g-mb-0--lg">
                            <h3 class="h5 g-color-black g-font-weight-500 mb-1">{{ $notice->title }}</h3>
                            {!! $notice->description !!}
                            {{-- <a class="d-inline-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#">
                            <i class="align-middle g-color-primary mr-2 icon-real-estate-027 u-line-icon-pro"></i>
                            Unify Perkins Lecture Hall
                            </a> --}}
                        </div>
                    </div>
                    <a class="text-uppercase btn btn-xs u-btn-primary rounded-0" href="#">{{$notice->type}}</a>
                </li>
                <!-- End Events Item -->
            @endforeach
        </ul>
        <nav class="text-center" aria-label="Page Navigation">
            {!! $notices->links('frontend.partials.paginate') !!}  
        </nav>
    </div>
</section>
      <!-- End More Events List -->
@endsection