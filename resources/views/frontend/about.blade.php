@extends('layouts.app')
@section('title','About Us')

@section('body')
<section class="g-my-40">
    <div class="container">
        <div class="text-center">
            <h4 class="g-mb-20">Title 1</h4>
        </div>
        <div class="g-mb-60">
            <p> Hundreds of millions of online accounts were compromised in corporate data breaches, the US presidential
              election process was plagued by cyber-meddling, internet infrastructure companies faced massive digital attacks that disrupted web connectivity for millions of people, and Apple faced off with the FBI in an epic privacy battle. When reality
              is more terrifying than a movie could ever be, perhaps it's time to retreat into the fictional for awhile and enjoy the fascinating world of cybersecurity from the safety of your living room for a change. Here are some favorite movies and
              television shows from 2016 that will let you get a taste of the hacker underworld over the holidays—hopefully without consigning you to a permanent state of digital dread. We aim high at being focused on building relationships with our clients
              and community. Using our creative gifts drives this foundation.</p>
          </div>

    </div>
</section>
@endsection