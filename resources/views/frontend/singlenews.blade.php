@extends('layouts.app')

@section('title',$news->title)

@section('body')

    <section class="g-my-40">
        <div class="container">
            <div class="row">
                <article class="g-mb-60">
                    <header class="g-mb-30">
                        <h2 class="h1 g-mb-15">{{ $news->title }}</h2>
            
                        <ul class="list-inline d-sm-flex g-color-gray-dark-v4 mb-0">
                            {{-- <li class="list-inline-item">
                                <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">Mike Coolman</a>
                            </li>
                            <li class="list-inline-item g-mx-10">/</li> --}}
                            <li class="list-inline-item">
                                {{ date('M j, Y',strtotime($news->published_date)) }}
                            </li>
                        </ul>
        
                        {{-- <hr class="g-brd-gray-light-v4 g-my-15">
        
                        <ul class="list-inline text-uppercase mb-0">
                            <li class="list-inline-item g-mr-10">
                                <a class="btn u-btn-facebook g-font-size-12 rounded g-px-20--sm g-py-10" href="#!">
                                    <i class="fa fa-facebook g-mr-5--sm"></i> <span class="g-hidden-xs-down">Share on Facebook</span>
                                </a>
                            </li>
                            <li class="list-inline-item g-mr-10">
                                <a class="btn u-btn-twitter g-font-size-12 rounded g-px-20--sm g-py-10" href="#!">
                                    <i class="fa fa-twitter g-mr-5--sm"></i> <span class="g-hidden-xs-down">Tweet on Twitter</span>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn u-btn-lightred g-font-size-12 rounded g-py-10" href="#!">
                                    <i class="fa fa-pinterest"></i>
                                </a>
                            </li>
                        </ul> --}}
                    </header>
            
                    <div class="g-font-size-16 g-line-height-1_8 g-mb-30">
                        <figure class=" g-mb-30">
                            <img class="img-fluid" src="{{ asset('news_images/'.$news->image) }}" alt="{{ $news->title }}">
                        </figure>  
                    </div>
            
                    {!! $news->description !!}
                </article>
            </div>

        </div>
    </section>
@endsection