<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Babai Irrigation Project Detail Report</title>
        <meta charset="UTF-8">
        <meta name=description content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <style>
            body {margin: 20px}
            thead {display: table-header-group;}
            tfoot {display: table-header-group;}
        </style>
    </head>
    <body>
        <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th colspan="1">SN</th>
                    <th colspan="1">Contract Id</th>
                    <th colspan="1"> Name of Work</th>
                    <th colspan="1">Estimated Amount</th>
                    <th colspan="3">Details of Contaract Agreement</th>
                    <th colspan="1">Due Date of Completion</th>
                    <th colspan="1">Bill Amount till Date</th>
                    <th colspan="1">Payment Due</th>
                    <th colspan="1">Financial Progress</th>
                    <th colspan="2">Power of Attorney Holder</th>
                    <th colspan="2">Mobilization</th>
                    <th colspan="2">PBG</th>
                    <th colspan="2">APG</th>
                    <th colspan="1">Remarks</th>
                    <th colspan="1">Side SDE</th>
                </tr>
                <tr>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col">Nrs.</th>
                    <th scope="col">Name of Contractor</th>
                    <th scope="col">Date</th>
                    <th scope="col">Amount (Nrs.)</th>
                    <th scope="col"></th>
                    <th scope="col"> </th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col">Name</th>
                    <th scope="col">Tel No</th>
                    <th scope="col">Total Nrs.</th>
                    <th scope="col">Due Nrs.</th>
                    <th scope="col">Amount Nrs.</th>
                    <th scope="col">Date</th>
                    <th scope="col">Amount Nrs.</th>
                    <th scope="col">Date</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
            </thead>
        
            <tbody>
                @foreach ($reports as $report)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $report->contract_id }}</td>
                        <td>{{ $report->details }}</td>
                        <td>{{ $report->estimated_amount }}</td>
                        <td>{{ $report->contractor_name }}</td>
                        <td>{{ $report->contract_date }}</td>
                        <td>{{ $report->contract_amount }}</td>
                        <td>{{ $report->due_date_completion }}</td>
                        <td>{{ $report->bill_amount_till_date }}</td>
                        <td>{{ $report->payment_due }}</td>
                        <td>{{ $report->financial_progress }}</td>
                        <td>{{ $report->attorney_holder_name }}</td>
                        <td>{{ $report->attorney_holder_tel_no }}</td>
                        <td>{{ $report->mobilization_total }}</td>
                        <td>{{ $report->mobilization_due }}</td>
                        <td>{{ $report->PGB_amount }}</td>
                        <td>{{ $report->PGB_date }}</td>
                        <td>{{ $report->APG_amount }}</td>
                        <td>{{ $report->APG_date }}</td>
                        <td>{{ $report->remark_date }}</td>
                        <td>
                            {{ $report->side_sdk }}
                        </td>
                    </tr>
                @endforeach
                
            </tbody>
        </table>
    </body>

    <script>
        var css = '@page { size: legal landscape; }',
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');

        style.type = 'text/css';
        style.media = 'print';

        if (style.styleSheet){
        style.styleSheet.cssText = css;
        } else {
        style.appendChild(document.createTextNode(css));
        }

        head.appendChild(style);
        window.print();
    </script>
</html>
