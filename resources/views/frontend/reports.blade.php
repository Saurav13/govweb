@extends('layouts.app')

@section('title',trans('labels.nav.reports'))

@section('body')
<style>
    th{
        border:1px solid #e9ecef;
        
    }
    td{
        border:1px solid #e9ecef;
    }
</style>
<section class="g-my-40">
    <div class="container">
        
        <div class="text-center mb-4">
                
            <h4 class="g-mb-20">Babai Irrigation Project Detail Reports:</h4>
            <a href="/report/print" class="btn btn-primary g-hidden-md-down" target="_blank">Print</a>
          <br>
            <div class="">
                <br>
                    <form action="/reports/filter" method="GET">
                    <select name="type1" required>
                            <option selected disabled>Choose Period</option>
    
                        <option value="Annual">Annual</option>
                        <option value="Quaterly">Quaterly</option>
                        <option value="Monthly">Monthly</option>
                        <option value="Weekly">Weekly</option>
                    </select>
                    <select name="type2" required>
                            <option selected disabled>Choose Type</option>
    
                        <option value="Physical">Physical</option>
                        <option value="Financial">Financial</option>
                    </select>
                    <input type="submit" class="btn btn-primary btn-sm" value="Filter">
                    </form>
                </div>
                <br>
            <br>
        </div>

    </div>
    <div style="margin: 0 2rem;">
        <div class="table-responsive" style="overflow:visible; width:100%; overflow-x:auto;">
            <table class="table">
                <thead>
                        <col>
                        <colgroup span="1"></colgroup>
                        <colgroup span="1"></colgroup>
                        <colgroup span="1"></colgroup>
                        <colgroup span="3"></colgroup>
                    <tr>
                        <th colspan="1">SN</th>
                        <th colspan="1">Contract Id</th>
                        <th colspan="1"> Name of Work</th>
                        <th colspan="1">Estimated Amount</th>
                        <th colspan="3">Details of Contaract Agreement</th>
                        <th colspan="1">Due Date of Completion</th>
                        <th colspan="1">Bill Amount till Date</th>
                        <th colspan="1">Payment Due</th>
                        <th colspan="1">Financial Progress</th>
                        <th colspan="2">Power of Attorney Holder</th>
                        <th colspan="2">Mobilization</th>
                        <th colspan="2">PBG</th>
                        <th colspan="2">APG</th>
                        <th colspan="1">Remarks</th>
                        <th colspan="1">Side SDE</th>
                    </tr>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col">Nrs.</th>
                        <th scope="col">Name of Contractor</th>
                        <th scope="col">Date</th>
                        <th scope="col">Amount (Nrs.)</th>
                        <th scope="col"></th>
                        <th scope="col"> </th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col">Name</th>
                        <th scope="col">Tel No</th>
                        <th scope="col">Total Nrs.</th>
                        <th scope="col">Due Nrs.</th>
                        <th scope="col">Amount Nrs.</th>
                        <th scope="col">Date</th>
                        <th scope="col">Amount Nrs.</th>
                        <th scope="col">Date</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
            
                <tbody>
                    @foreach ($reports as $report)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $report->contract_id }}</td>
                            <td>{{ $report->details }}</td>
                            <td>{{ $report->estimated_amount }}</td>
                            <td>{{ $report->contractor_name }}</td>
                            <td>{{ $report->contract_date }}</td>
                            <td>{{ $report->contract_amount }}</td>
                            <td>{{ $report->due_date_completion }}</td>
                            <td>{{ $report->bill_amount_till_date }}</td>
                            <td>{{ $report->payment_due }}</td>
                            <td>{{ $report->financial_progress }}</td>
                            <td>{{ $report->attorney_holder_name }}</td>
                            <td>{{ $report->attorney_holder_tel_no }}</td>
                            <td>{{ $report->mobilization_total }}</td>
                            <td>{{ $report->mobilization_due }}</td>
                            <td>{{ $report->PGB_amount }}</td>
                            <td>{{ $report->PGB_date }}</td>
                            <td>{{ $report->APG_amount }}</td>
                            <td>{{ $report->APG_date }}</td>
                            <td>{{ $report->remark_date }}</td>
                            <td>
                                {{ $report->side_sdk }}
                                {{-- <span class="u-label u-label-warning g-color-white">Expiring</span> --}}
                            </td>
                        </tr>
                        
                    @endforeach
                    
                </tbody>
            </table>
        </div>    
    </div>
</section>
      <!-- End More Events List -->
@endsection