@extends('layouts.app')

@section('title',trans('labels.nav.contact'))

@section('body')
<div class="container">
    <br>

    <div class="g-my-60">
        <div class="u-heading-v3-1 g-mb-30">
            <h2 class="h5 u-heading-v3__title g-color-gray-dark-v1 text-uppercase g-brd-primary">@lang('labels.general.leave_a_message')</h2>
        </div>

        <form method="POST" action="{{ route('contactSend') }}">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6 form-group g-mb-30">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-pa-10" type="text" value="{{ old('name') }}" placeholder="Your Name" name="name" required>
                </div>

                <div class="col-md-6 form-group g-mb-30">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-pa-10" type="email" value="{{ old('email') }}" placeholder="Email" name="email" required>
                </div>

                <div class="col-md-12 form-group g-mb-30">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-pa-10" type="text" value="{{ old('subject') }}" placeholder="Subject" name="subject" required>
                </div>
            </div>

            <div class="g-mb-30">
                <textarea class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus g-resize-none rounded-3 g-pa-15" rows="12" placeholder="Your Message" name="message" required>{{ old('message') }}</textarea>
            </div>

            <button class="btn u-btn-primary g-font-size-12 text-uppercase g-px-25 g-py-13" type="submit">
                <i class="icon-finance-206 u-line-icon-pro align-middle g-pos-rel g-top-1 mr-2"></i> @lang('labels.general.send')
            </button>
        </form>
    </div>
</div>

@if(Session::has('contact_success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert" style="position: fixed;right: 2rem; bottom: 2px;z-index: 2;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <h4 class="h5">
            <i class="fa fa-check-circle-o"></i>
            Success!
        </h4>
        @lang('labels.general.contact_success')
    </div>
@endif

@if(Session::has('contact_error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="position: fixed;right: 2rem; bottom: 2px;    z-index: 2;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <h4 class="h5">
            <i class="fa fa-check-circle-o"></i>
            Error!
        </h4>
        @lang('labels.general.contact_error')        
    </div>
@endif

@stop