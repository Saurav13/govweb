@extends('layouts.app')

@section('title',trans('labels.nav.news'))

@section('body')

    <section class="g-my-40">
        <div class="container">
            <div class="text-center">
                <h4 class="g-mb-20">Latest Updates</h4>
            </div>
            <div class="row">
                @foreach ($newsList as $news)
                    <div class="col-lg-4 g-mb-30">
                        <!-- Article -->
                        <article class="g-bg-white">
                            <figure class="g-pos-rel">
                                <img class="img-fluid w-100" src="{{ route('optimize', ['news_images',$news->image,350,300]) }}" alt="{{ $news->title }}">
                                <figcaption class="text-uppercase text-center g-line-height-1_2 g-bg-primary-opacity-0_8 g-color-white g-pos-abs g-top-20 g-px-15 g-py-10">
                                    <strong class="d-block">{{ date('j M',strtotime($news->published_date)) }}</strong>
                                    <hr class="g-brd-white my-1">
                                    <small class="d-block">{{ date('Y',strtotime($news->published_date)) }}</small>
                                </figcaption>
                            </figure>
        
                            <div class="g-pa-30">
                                <h3 class="h5 g-mb-15">
                                    <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover" href="/news/{{ $news->slug }}">{{ $news->title }}</a>
                                </h3>
        
                                <p>
                                    {{ (str_limit(strip_tags($news->description), $limit = 170, $end = '...')) }}
                                </p>
        
                                {{-- <hr class="g-brd-gray-light-v4 g-my-20">
        
                                <ul class="list-inline small text-uppercase mb-0">
                                    <li class="list-inline-item">
                                        <span class="g-color-gray-dark-v4">By</span>
                                        <a class="u-link-v5 g-color-main g-color-primary--hover" href="#">Kathy Reyes</a>
                                    </li>
                                    <li class="list-inline-item">-</li>
                                    <li class="list-inline-item">
                                        <i class="icon-bubbles align-middle g-color-gray-dark-v4 g-mr-2"></i>
                                        32 commetns
                                    </li>
                                </ul> --}}
                            </div>
                        </article>
                        <!-- End Article -->
                    </div>
                @endforeach
                
            </div>
            <nav class="text-center" aria-label="Page Navigation">
                {!! $newsList->links('frontend.partials.paginate') !!}  
            </nav>

        </div>
    </section>
@endsection