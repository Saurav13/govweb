@extends('layouts.app')

@section('title',$album->name)

@section('body')

        
        
</div>
<section class="g-my-40">
  <div class="container">
    <div class="text-center">
    <h4 class="g-mb-20"><a href="/albums">Albums </a>| {{$album->name}}</h4>
    </div>
    <div class="row">
        @foreach($images as $image)
            <div class="col-md-4 g-mb-30">
                <a class="js-fancybox d-block u-block-hover" href="javascript:;" data-fancybox="lightbox-gallery--03" data-src="/gallery/{{$image->image}}" data-speed="350" data-caption="{{ $image->caption }}">
                    <img class="img-fluid u-block-hover__main--mover-right" src="/gallery/{{$image->image}}" alt="Image Description">
                </a>
                <header class="u-bg-overlay__inner g-pos-abs g-bottom-30 g-right-30 g-left-30 g-color-white">
                    <h3 class="h4">{{ $image->caption }}</h3>
                </header>
            </div>
        @endforeach
       
    </div>
     <!-- Pagination -->
    <nav class="g-mt-50 text-center" aria-label="Page Navigation">
        {{ $images->appends(request()->except('page'))->links('frontend.partials.pagination') }}
    </nav>
    <!-- End Pagination -->

    </div>
</section>

@endsection
