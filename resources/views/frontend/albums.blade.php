@extends('layouts.app')

@section('title',trans('labels.nav.gallery'))

@section('body')
<section class="g-my-40">
  <div class="container">
   <div class="text-center">
        <h4 class="g-mb-20">Albums</h4>
    </div>
    <div class="row">
      @foreach($albums as $album)
      @if($album->album_images->count()>0)
      <div class="col-md-6 g-mb-30--md">
        <article class="u-block-hover g-rounded-6">
          <figure class="u-bg-overlay g-bg-black-opacity-0_4--after">
            <img class="img-fluid u-block-hover__main--zoom-v1" src="/gallery/{{$album->album_images->first()->image}}" alt="Image Description">
          </figure>
          <header class="u-bg-overlay__inner g-pos-abs g-top-30 g-right-30 g-left-30 g-color-white">
            <h3 class="h4 g-color-white">{{$album->name}}</h3>
            {{$album->year}}
          </header>
          
          <a class="u-link-v2" href="/albums/{{$album->slug}}" >Read More
          </a>
        </article>
      </div>
      @endif
      @endforeach
    </div>
    <nav class="g-mt-50 text-center" aria-label="Page Navigation">
        {{ $albums->appends(request()->except('page'))->links('frontend.partials.pagination') }}
    </nav>
  </div>
</section>

@endsection

@section('js')
{{-- <script  src="/frontend/main-assets/assets/js/components/hs.popup.js"></script> --}}

<!-- JS Plugins Init. -->
<script >
  $(document).on('ready', function () {
    // initialization of popups
    // $.HSCore.components.HSPopup.init('.js-fancybox');
  });
</script>
@endsection