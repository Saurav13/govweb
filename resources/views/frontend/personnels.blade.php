@extends('layouts.app')

@section('title',trans('labels.nav.personnels'))

@section('body')

<section class="g-my-40 g-mx-20">
        <div class="row">

            <div class="col-sm-12 col-lg-8 col-md-8">
                  <div class="text-center">
                      <h4 class="g-mb-40">Personnels</h4>
                  </div>
                  <div id="carouselCus3" class="js-carousel g-mb-50--lg" data-infinite="true" data-fade="true" data-lazy-load="ondemand" data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-width-45 g-height-45 g-font-size-30 g-color-gray-dark-v4 g-color-primary--hover" data-arrow-left-classes="fa fa-angle-left g-left-0 g-left-40--lg" data-arrow-right-classes="fa fa-angle-right g-right-0 g-right-40--lg" data-nav-for="#carouselCus4">
                    @foreach($personnels as $person)  
                    <div class="js-slide">
                        <!-- Team -->
                        <div class="row justify-content-center align-items-center no-gutters">
                        <div class="col-sm-6 col-lg-4 g-bg-size-cover g-bg-pos-top-center g-min-height-400" data-bg-img-src="/personnel_images/{{$person->image}}"></div>
                          <div class="col-sm-6 col-lg-4">
                            <div class="g-px-3 g-px-30--lg g-py-60">
                              <h3 class="h4 mb-1">{{$person->name}}</h3>
                              <span class="d-block">{{$person->designation}}</span>
                              <span class="d-block mb-4">{{$person->department}}</span>

                            <p class="mb-4">{{$person->description}}</p>
                    
                            </div>
                          </div>
                        </div>
                        <!-- End Team -->
                      </div>
                      @endforeach
                    
                  </div>
                    
                  <div id="carouselCus4" class="js-carousel text-center u-carousel-v3 g-mx-minus-15" data-infinite="true" data-center-mode="true" data-slides-show="5" data-is-thumbs="true" data-lazy-load="ondemand" data-nav-for="#carouselCus3">
                   @foreach($personnels as $person)
                    <div class="js-slide g-opacity-1 g-cursor-pointer g-px-15">
                    <img class="img-fluid mb-3" src="/personnel_images/{{$person->image}}" alt="{{$person->name}}">
                      <h3 class="h6 g-color-gray-dark-v4">{{$person->name}}</h3>
                    </div>
                    @endforeach
                  
                  
                  </div>
              </div>

              <div class="col-md-3 g-ml-30 g-mt-10" >
                  <div class="text-center g-brd-primary" style="border:1px solid; padding:1rem; border-radius:10px">
                      <h4 class="g-mb-20">Project Report</h4>

                      <a href="/reports" class="btn btn-primary">View Report.</a>
                  </div>
                  <hr>

                  <div class="text-center">
                      <h4 class="g-mb-20">Notices</h4>
                  </div>
                  <ul class="list-unstyled">
                    @foreach($notices as $notice)
                      <li class="g-brd-around g-brd-gray-light-v4 g-pa-20 g-mb-minus-1">
                        <div class="media">
                          <div class="media-body">
                          <p class="m-0">{{$notice->title}}</p>
                            <span class="g-font-size-12 g-color-gray">{{$notice->published_date}}</span>
                          </div>
                        </div>
                      </li>
                      @endforeach
                    </ul>
                    <div class="text-center">
                        <a href="/notices" class="btn btn-primary">View All.</a>

                    </div>
                    

              </div>
        

        </div>
      </section>
@endsection