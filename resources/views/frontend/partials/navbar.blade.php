<style>
  .menu_hover{
    background:#1b5693;
  }
  .menu_hover:hover{
    background:#194879;
  }
</style>
<body>

  <main>
    <style>
      .hamburger-inner{
        background:white !important;
      }
      .hamburger-inner:after{
        background:white !important;
      }
      .hamburger-inner:before{
        background:white !important;
      }
      .lang-lg{

      }
      @media only screen and (min-width:600px){
        .lang-lg{
          position:absolute;
          right:0
        }
      }

      @media only screen and (min-width:768px){
        .lang-lg{
          position:absolute;
          right:0
        }
      }
      
    </style>
    <!-- Header -->
    <header id="js-header" class="u-header u-header--static u-shadow-v19">
      <!-- Top Bar -->
      <div class="u-header__section g-brd-bottom g-brd-gray-light-v4 g-py-10 g-hidden-md-down">
        <div class="container">
          <div class="row align-items-center">
            <!-- Logo -->
            <div class="col-md-3 g-hidden-md-down">
              <a href="/" class="navbar-brand">
                <img src="/nepal_logo.png" style="height:8rem;" alt="Logo">
              </a>
            </div>
            <!-- End Logo -->

            <!-- Subscribe Form -->
            <div class="col-md-6 col-sm-9 text-center" style="width:70%">

              <h4 style="font-size: 16px;">@lang('labels.headers.l1')</h4>
              <h4 style="font-size: 16px;">@lang('labels.headers.l2')</h4>
              <h4 style="font-size: 16px;">@lang('labels.headers.l3')</h4>
              <h4 style="font-size: 16px; font-weight: 600;">@lang('labels.headers.l4'), @lang('labels.headers.l5')</h4>
            
             </div>
            <!-- End Subscribe Form -->

            <!-- Account -->
            <div class="col-md-3 col-sm-2 " style="width:30%">
              <a id="account-dropdown-invoker" class="media align-items-center float-right g-text-underline--none--hover" href="#!"
                 aria-controls="account-dropdown"
                 aria-haspopup="true"
                 aria-expanded="false"
                 data-dropdown-event="hover"
                 data-dropdown-target="#account-dropdown"
                 data-dropdown-type="css-animation"
                 data-dropdown-duration="300"
                 data-dropdown-hide-on-scroll="false"
                 data-dropdown-animation-in="fadeIn"
                 data-dropdown-animation-out="fadeOut">
                <div class=" mr-2">
                  <img class="img-fluid" src="/flag.gif" style="height:8rem;" alt="Image Description">
                </div>
              </a>
            </div>
            <!-- End Account -->
          </div>
        </div>
      </div>
      <!-- End Top Bar -->

      <div class="u-header__section u-header__section--light g-bg-primary g-transition-0_3 g-py-10">
        <nav class="js-mega-menu navbar navbar-expand-lg g-px-0">
          <div class="container g-px-15">
            <!-- Logo -->
            <a class="navbar-brand g-hidden-lg-up" href="/">
              <img src="/nepal_logo.png" style="height:4rem;" alt="Logo">
            </a>
            <!-- End Logo -->

            <!-- Responsive Toggle Button -->
            <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 ml-auto" type="button"
                    aria-label="Toggle navigation"
                    aria-expanded="false"
                    aria-controls="navBar"
                    data-toggle="collapse"
                    data-target="#navBar">
              <span class="hamburger hamburger--slider g-pa-0">
                <span class="hamburger-box">
                  <span class="hamburger-inner"></span>
                </span>
              </span>
            </button>
            <!-- End Responsive Toggle Button -->

            <!-- Navigation -->
            <div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--lg" id="navBar">
              <ul class="navbar-nav g-font-weight-600">
                <!-- Home - Submenu -->
                <li class="nav-item g-mx-10--lg g-mx-20--xl">
                  <a id="nav-link--pages" class="nav-link text-uppercase g-color-white g-px-0" href="/">
                    @lang('labels.nav.home')
                  </a>
                </li>
                <li class="nav-item hs-has-sub-menu g-mr-10--lg g-mr-20--xl">
                  <a id="nav-link--info" class="nav-link text-uppercase g-color-white g-px-0" href="#"
                     aria-haspopup="true"
                     aria-expanded="false"
                     aria-controls="nav-submenu--info">
                    @lang('labels.nav.about')
                  </a>

                  <!-- Submenu -->
                  <ul id="nav-submenu--info" class="hs-sub-menu list-unstyled u-shadow-v11 g-min-width-220 g-brd-top g-brd-primary g-brd-top-2 g-mt-17" aria-labelledby="nav-link--info">
                    
                    @foreach ($infos as $info)
                    @if($info->slug != "emc" && $info->slug != "wmc")
                      <li class="dropdown-item menu_hover">
                        <a class="nav-link g-color-white" href="{{ URL::to($info->slug) }}">{{ $info->title }}</a>
                      </li>
                      @endif
                    @endforeach
                    <li class="dropdown-item hs-has-sub-menu menu_hover">
                      <a id="nav-link--pages--about" class="nav-link g-color-white" href="#" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu--pages--about">Water Users Organization</a>

                      <!-- Submenu (level 2) -->
                      <ul class="hs-sub-menu list-unstyled u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-minus-2 animated" id="nav-submenu--pages--about" aria-labelledby="nav-link--pages--about" style="display: none;">
                        <li class="dropdown-item menu_hover ">
                          <a class="nav-link g-color-white" href="/emc">EMC</a>
                        </li>
                        <li class="dropdown-item menu_hover">
                          <a class="nav-link g-color-white" href="/wmc">WMC</a>
                        </li>
                    
             
                      </ul>
                      <!-- End Submenu (level 2) -->
                    </li>
                    <li class="dropdown-item menu_hover">
                        <a class="nav-link g-color-white" href="/citizen-charter">@lang('labels.nav.employees')</a>
                     
                    </li>
                    <li class="dropdown-item menu_hover">
                      <a class="nav-link g-color-white" href="/citizen-charter">@lang('labels.nav.citizenCharter')</a>
                    </li>
                  </ul>
                  <!-- End Submenu -->
                </li>
                
                <li class="nav-item g-mx-10--lg g-mx-20--xl">
                  <a id="nav-link--pages" class="nav-link text-uppercase g-color-white g-px-0" href="/notices">
                    @lang('labels.nav.notices')
                  </a>
                </li>
                <li class="nav-item g-mx-10--lg g-mx-20--xl">
                  <a id="nav-link--pages" class="nav-link text-uppercase g-color-white g-px-0" href="/news">
                    @lang('labels.nav.news')
                  </a>
                </li>
                <li class="nav-item g-mx-10--lg g-mx-20--xl">
                  <a id="nav-link--pages" class="nav-link text-uppercase g-color-white g-px-0" href="/projects">
                    @lang('labels.nav.projects')
                  </a>
                </li>
                <li class="nav-item g-mx-10--lg g-mx-20--xl">
                  <a id="nav-link--pages" class="nav-link text-uppercase g-color-white g-px-0" href="/reports">            
                    @lang('labels.nav.reports')
                  </a>
                </li>
                <li class="nav-item g-mx-10--lg g-mx-20--xl">
                  <a id="nav-link--pages" class="nav-link text-uppercase g-color-white g-px-0" href="/albums">
                    @lang('labels.nav.gallery')
                  </a>
                </li>
                <li class="nav-item g-mx-10--lg g-mx-20--xl">
                  <a id="nav-link--pages" class="nav-link text-uppercase g-color-white g-px-0" href="/contact-us">
                    @lang('labels.nav.contact')
                  </a>
                </li>


                <!-- End Home - Submenu -->

                <!-- End Main -->
              </ul>
              <div class="lang-lg">
                  <a id="languages-dropdown-invoker" class="g-color-white g-text-underline--none--hover" href="#!"
                    aria-controls="languages-dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    data-dropdown-event="hover"
                    data-dropdown-target="#languages-dropdown"
                    data-dropdown-type="css-animation"
                    data-dropdown-duration="300"
                    data-dropdown-hide-on-scroll="false"
                    data-dropdown-animation-in="fadeIn"
                    data-dropdown-animation-out="fadeOut">
                    @if(app()->getLocale() == 'en')
                      <svg xmlns="http://www.w3.org/2000/svg" height="11" width="27" viewBox="0 0 640 480">
                        <defs>
                          <clipPath id="a">
                            <path fill-opacity=".67" d="M-85.333 0h682.67v512h-682.67z"/>
                          </clipPath>
                        </defs>
                        <g clip-path="url(#a)" transform="translate(80) scale(.94)">
                          <g stroke-width="1pt">
                            <path fill="#006" d="M-256 0H768.02v512.01H-256z"/>
                            <path d="M-256 0v57.244l909.535 454.768H768.02V454.77L-141.515 0H-256zM768.02 0v57.243L-141.515 512.01H-256v-57.243L653.535 0H768.02z" fill="#fff"/>
                            <path d="M170.675 0v512.01h170.67V0h-170.67zM-256 170.67v170.67H768.02V170.67H-256z" fill="#fff"/>
                            <path d="M-256 204.804v102.402H768.02V204.804H-256zM204.81 0v512.01h102.4V0h-102.4zM-256 512.01L85.34 341.34h76.324l-341.34 170.67H-256zM-256 0L85.34 170.67H9.016L-256 38.164V0zm606.356 170.67L691.696 0h76.324L426.68 170.67h-76.324zM768.02 512.01L426.68 341.34h76.324L768.02 473.848v38.162z" fill="#c00"/>
                          </g>
                        </g>
                      </svg>
                      <span>English</span> <i class="g-hidden-sm-down fa fa-angle-down g-ml-7"></i>
                    @else
                      <svg xmlns="http://www.w3.org/2000/svg" width="27" height="11" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-17.582 -4.664 71.571 87.246">
                        <use stroke="#003893" stroke-width="5.165" xlink:href="#a"/>
                        <path id="a" fill="#DC143C" d="M-15 37.57h60L-15 0v80h60l-60-60z"/>
                        <g fill="#fff">
                        <path d="M0 35.43c6.6 0 11.95-5.35 11.95-11.95C10.9 26.15 9 28.36 6.58 29.8l-1.02-.76 2-1.5-2.42-.62 1.28-2.17-2.5.36.37-2.48-2.17 1.3-.62-2.45-1.5 2-1.5-2-.63 2.44-2.16-1.28.37 2.5-2.5-.37 1.3 2.17-2.44.62 2 1.5-1 .77c-2.37-1.4-4.3-3.56-5.38-6.32 0 6.6 5.35 11.95 11.95 11.95z"/><path d="M-5.76 53.03l-5.36-.66 3.25 4.3-4.97 2.12 4.97 2.1-3.25 4.3 5.36-.66-.66 5.37 4.3-3.25L0 71.63l2.1-4.98 4.32 3.26-.66-5.36 5.36.67-3.25-4.3 4.97-2.1-4.97-2.12 3.25-4.3-5.36.65.66-5.36-4.3 3.25L0 45.95l-2.1 4.97-4.32-3.25.66 5.36z"/></g>
                      </svg>
                      <span>नेपाली</span> <i class="g-hidden-sm-down fa fa-angle-down g-ml-7"></i>
    
                    @endif
                  </a>
    
                  <ul id="languages-dropdown" class="list-unstyled g-width-160 g-brd-around g-brd-secondary-light-v2 g-bg-white rounded g-pos-abs g-py-5 g-mt-32"
                      aria-labelledby="languages-dropdown-invoker">
                    <li>
                      <a class="d-block g-color-secondary-dark-v1 g-color-primary--hover g-text-underline--none--hover g-py-5 g-px-20" href="{{ url('locale/en') }}">
                        <svg class="mr-1 g-ml-minus-6" xmlns="http://www.w3.org/2000/svg" height="11" width="27" viewBox="0 0 640 480">
                          <defs>
                            <clipPath id="a">
                              <path fill-opacity=".67" d="M-85.333 0h682.67v512h-682.67z"/>
                            </clipPath>
                          </defs>
                          <g clip-path="url(#a)" transform="translate(80) scale(.94)">
                            <g stroke-width="1pt">
                              <path fill="#006" d="M-256 0H768.02v512.01H-256z"/>
                              <path d="M-256 0v57.244l909.535 454.768H768.02V454.77L-141.515 0H-256zM768.02 0v57.243L-141.515 512.01H-256v-57.243L653.535 0H768.02z" fill="#fff"/>
                              <path d="M170.675 0v512.01h170.67V0h-170.67zM-256 170.67v170.67H768.02V170.67H-256z" fill="#fff"/>
                              <path d="M-256 204.804v102.402H768.02V204.804H-256zM204.81 0v512.01h102.4V0h-102.4zM-256 512.01L85.34 341.34h76.324l-341.34 170.67H-256zM-256 0L85.34 170.67H9.016L-256 38.164V0zm606.356 170.67L691.696 0h76.324L426.68 170.67h-76.324zM768.02 512.01L426.68 341.34h76.324L768.02 473.848v38.162z" fill="#c00"/>
                            </g>
                          </g>
                        </svg>
                        English
                      </a>
                    </li>
                    <li>
                      <a class="d-block g-color-secondary-dark-v1 g-color-primary--hover g-text-underline--none--hover g-py-5 g-px-20" href="{{ url('locale/ne') }}">
    
                        <svg class="mr-1 g-ml-minus-6" id="nepalFlag" xmlns="http://www.w3.org/2000/svg" width="27" height="11" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-17.582 -4.664 71.571 87.246">
                          <use stroke="#003893" stroke-width="5.165" xlink:href="#nepalFlag"/>
                          <path id="a" fill="#DC143C" d="M-15 37.57h60L-15 0v80h60l-60-60z"/>
                          <g fill="#fff">
                          <path d="M0 35.43c6.6 0 11.95-5.35 11.95-11.95C10.9 26.15 9 28.36 6.58 29.8l-1.02-.76 2-1.5-2.42-.62 1.28-2.17-2.5.36.37-2.48-2.17 1.3-.62-2.45-1.5 2-1.5-2-.63 2.44-2.16-1.28.37 2.5-2.5-.37 1.3 2.17-2.44.62 2 1.5-1 .77c-2.37-1.4-4.3-3.56-5.38-6.32 0 6.6 5.35 11.95 11.95 11.95z"/><path d="M-5.76 53.03l-5.36-.66 3.25 4.3-4.97 2.12 4.97 2.1-3.25 4.3 5.36-.66-.66 5.37 4.3-3.25L0 71.63l2.1-4.98 4.32 3.26-.66-5.36 5.36.67-3.25-4.3 4.97-2.1-4.97-2.12 3.25-4.3-5.36.65.66-5.36-4.3 3.25L0 45.95l-2.1 4.97-4.32-3.25.66 5.36z"/></g>
                        </svg>
                        नेपाली
    
                      </a>
                    </li>
                  </ul>
                </div>
                
            </div>
             <!-- Language -->
           
            <!-- End Language -->
            <!-- End Navigation -->
          </div>
        </nav>
      </div>
    </header>
    <!-- End Header -->
