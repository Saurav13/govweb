<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <!-- Title -->
    <title>Babai Irrigation | @yield('title')</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/frontend-assets/main-assets/favicon.ico">

    <!-- Google Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto+Slab%3A400%2C300%2C700%7CPlayfair+Display%7CRoboto%7CRaleway%7CSpectral%7CRubik">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-line/css/simple-line-icons.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-line-pro/style.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-hs/style.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/fancybox/jquery.fancybox.css">

    <!-- CSS Unify Theme -->
    <link rel="stylesheet" href="/frontend-assets/assets/css/styles.bm-classic.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/master-slider/source/assets/css/masterslider.main.css">


    <!-- CSS Customization -->
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/custom.css">
    <link rel="stylesheet" href="/frontend-assets/assets/css/style.css">


    @yield('css')
  </head>
