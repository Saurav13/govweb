  <!-- Footer -->
  <footer class="g-bg-secondary">
      <div class="g-bg-black-opacity-0_9 g-color-white-opacity-0_8 g-py-60">
          <div class="container">
            <div class="row">
              <!-- Footer Content -->
              <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
                <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                  <h2 class="u-heading-v2__title h6 text-uppercase mb-0">About Us</h2>
                </div>
        
                <p>@lang('labels.headers.l1')<br>
                   @lang('labels.headers.l2')<br>
                   @lang('labels.headers.l3')<br>
                   @lang('labels.headers.l4')<br>
                   @lang('labels.headers.l5')
                </p>
              </div>
              <!-- End Footer Content -->
        
              <!-- Footer Content -->
              <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
                <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                  <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Info Links</h2>
                </div>
                <nav class="text-uppercase1">
                  <ul class="list-unstyled g-mt-minus-10 mb-0">
                    @foreach ($infos as $info)

                      <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                        <h4 class="h6 g-pr-20 mb-0">
                          <a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ URL::to($info->slug) }}">{{$info->title}}</a>
                          <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                        </h4>
                      </li>
                    @endforeach
                     <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                      <h4 class="h6 g-pr-20 mb-0">
                        <a class="g-color-white-opacity-0_8 g-color-white--hover" href="/citizen-charter">@lang('labels.nav.citizenCharter')</a>
                        <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                      </h4>
                    </li>
                  </ul>
                </nav>
               
              </div>
              <!-- End Footer Content -->
        
              <!-- Footer Content -->
              <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
                <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                  <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Useful Links</h2>
                </div>
        
                <nav class="text-uppercase1">
                  <ul class="list-unstyled g-mt-minus-10 mb-0">
                   
                    <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                      <h4 class="h6 g-pr-20 mb-0">
                        <a class="g-color-white-opacity-0_8 g-color-white--hover" href="/notices">@lang('labels.nav.notices')</a>
                        <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                      </h4>
                    </li>
                    <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                      <h4 class="h6 g-pr-20 mb-0">
                        <a class="g-color-white-opacity-0_8 g-color-white--hover" href="/news">@lang('labels.nav.news') </a>
                        <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                      </h4>
                    </li>
                    <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                      <h4 class="h6 g-pr-20 mb-0">
                        <a class="g-color-white-opacity-0_8 g-color-white--hover" href="/projects">@lang('labels.nav.projects')</a>
                        <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                      </h4>
                    </li>
                    <li class="g-pos-rel g-py-10">
                      <h4 class="h6 g-pr-20 mb-0">
                        <a class="g-color-white-opacity-0_8 g-color-white--hover" href="/reports">@lang('labels.nav.reports')</a>
                        <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                      </h4>
                    </li>
                    <li class="g-pos-rel g-py-10">
                        <h4 class="h6 g-pr-20 mb-0">
                          <a class="g-color-white-opacity-0_8 g-color-white--hover" href="/albums">@lang('labels.nav.gallery')</a>
                          <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                        </h4>
                      </li>
                  </ul>
                </nav>
              </div>
              <!-- End Footer Content -->
        
              <!-- Footer Content -->
              <div class="col-lg-3 col-md-6">
                <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                  <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Our Contacts</h2>
                </div>
        
                <address class="g-bg-no-repeat g-font-size-12 mb-0" style="background-image: url(/frontend-assets/main-assets/assets/img/maps/map2.png);">
                  <!-- Location -->
                  <div class="d-flex g-mb-20">
                    <div class="g-mr-10">
                      <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                        <i class="fa fa-map-marker"></i>
                      </span>
                    </div>
                    <p class="mb-0">@lang('labels.contact.l1')
                      <br>
                      @lang('labels.contact.l2')
                      <br>
                      @lang('labels.contact.l3')
                    </p>
                  </div>
                  <!-- End Location -->
        
                  <!-- Phone -->
                  <div class="d-flex g-mb-20">
                    <div class="g-mr-10">
                      <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                        <i class="fa fa-phone"></i>
                      </span>
                    </div>
                    <p class="mb-0">@lang('labels.contact.l4')</p>
                  </div>
                  <!-- End Phone -->
        
                  <!-- Email and Website -->
                  <div class="d-flex g-mb-20">
                    <div class="g-mr-10">
                      <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                        <i class="fa fa-globe"></i>
                      </span>
                    </div>
                    <p class="mb-0">
                      <a class="g-color-white-opacity-0_8 g-color-white--hover" href="mailto:info@babaiip.gov.np">info@babaiip.gov.np</a>
                      
                    </p>
                  </div>
                  <!-- End Email and Website -->
                </address>
              </div>
              <!-- End Footer Content -->
            </div>
          </div>
        </div>
        <!-- End Footer -->
        
        <!-- Copyright Footer -->
        {{-- <footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20">
          <div class="container">
            <div class="row">
              <div class="col-md-8 text-center text-md-left g-mb-10 g-mb-0--md">
                <div class="d-lg-flex">
                  <small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md">2019 © All Rights Reserved.</small>
                  <ul class="u-list-inline">
                    <li class="list-inline-item">
                      <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">Privacy Policy</a>
                    </li>
                    <li class="list-inline-item">
                      <span>|</span>
                    </li>
                    <li class="list-inline-item">
                      <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">Terms of Use</a>
                    </li>
                    <li class="list-inline-item">
                      <span>|</span>
                    </li>
                    <li class="list-inline-item">
                      <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">License</a>
                    </li>
                    <li class="list-inline-item">
                      <span>|</span>
                    </li>
                    <li class="list-inline-item">
                      <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">Support</a>
                    </li>
                  </ul>
                </div>
              </div>
        
              <div class="col-md-4 align-self-center">
                <ul class="list-inline text-center text-md-right mb-0">
                  <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Facebook">
                    <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                      <i class="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Skype">
                    <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                      <i class="fa fa-skype"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Linkedin">
                    <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                      <i class="fa fa-linkedin"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Pinterest">
                    <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                      <i class="fa fa-pinterest"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Twitter">
                    <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                      <i class="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Dribbble">
                    <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                      <i class="fa fa-dribbble"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </footer> --}}
  </footer>
  <!-- End Footer -->

  <!-- End Go To -->
</main>

<div class="u-outer-spaces-helper"></div>

<!-- JS Global Compulsory -->
<script src="/frontend-assets/main-assets/assets/vendor/jquery/jquery.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/popper.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>

<!-- JS Implementing Plugins -->
<script src="/frontend-assets/main-assets/assets/vendor/appear.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/fancybox/jquery.fancybox.min.js"></script>

<!-- JS Unify -->
<script src="/frontend-assets/main-assets/assets/js/hs.core.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.header.js"></script>
<script src="/frontend-assets/main-assets/assets/js/helpers/hs.hamburgers.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.dropdown.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.counter.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.onscroll-animation.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.sticky-block.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.carousel.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.popup.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.go-to.js"></script>

<script src="/frontend-assets/main-assets/assets/vendor/master-slider/source/assets/js/masterslider.min.js"></script>

<!-- JS Customization -->
<script src="/frontend-assets/main-assets/assets/js/custom.js"></script>

<!-- JS Plugins Init. -->
<script>
  $(document).on('ready', function () {
    // initialization of header
    $.HSCore.components.HSHeader.init($('#js-header'));
    $.HSCore.helpers.HSHamburgers.init('.hamburger');

    // initialization of MegaMenu
    $('.js-mega-menu').HSMegaMenu();

    // initialization of HSDropdown component
    $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
      afterOpen: function () {
        $(this).find('input[type="search"]').focus();
      }
    });

    // initialization of scroll animation
    $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

    // initialization of go to
    $.HSCore.components.HSGoTo.init('.js-go-to');

    // initialization of counters
    var counters = $.HSCore.components.HSCounter.init('[class*="js-counter"]');

    // initialization of carousel
    $.HSCore.components.HSCarousel.init('[class*="js-carousel"]');

    // initialization of popups
    $.HSCore.components.HSPopup.init('.js-fancybox');

  });

  $(window).on('load', function () {
    // initialization of sticky blocks
    setTimeout(function() { // important in this case
      $.HSCore.components.HSStickyBlock.init('.js-sticky-block');
    }, 1);
  });
</script>

@yield('js')
</body>
</html>