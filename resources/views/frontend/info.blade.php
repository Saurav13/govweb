@extends('layouts.app')

@section('title',$info->title)

@section('body')
<section class="g-my-40">
    <div class="container">
        <div class="text-center">
            <h4 class="g-mb-20">{{ $info->title }}</h4>
        </div>
        <div class="g-mb-60">
            {!! $info->description !!}
        </div>
    
    </div>
</section>

@endsection