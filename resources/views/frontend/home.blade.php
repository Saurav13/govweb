@extends('layouts.app')

@section('title',trans('labels.nav.home'))

@section('css')
    <!-- Revolution Slider -->
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/css/settings.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/css/layers.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/css/navigation.css">
@endsection

@section('body')

  <style>

    .resp_title{
      font-size:13px !important;
    }

    @media only screen and (min-width:600px){
      .resp_title{
        font-size:32px !important;
      }
    }

    @media only screen and (min-width:768px){
      .resp_title{
        font-size:32px !important;
      }
    }
  </style>
 
     
 <div class="g-overflow-hidden">
    <div id="rev_slider_1014_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="typewriter-effect" data-source="gallery" style="background-color:transparent;padding:0px;">
      <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
      <div id="rev_slider_1014_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.1">
        <ul>  <!-- SLIDE  -->
          <li data-index="rs-2800" data-transition="curtain-1" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="img1.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
            <!-- MAIN IMAGE -->
            <img src="/img1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" class="rev-slidebg">
            <!-- LAYERS -->

            <!-- LAYER NR. 1 -->
            <div class="tp-caption tp-shape tp-shapewrapper "
                 id="slide-2800-layer-7"
                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                 data-width="full"
                 data-height="full"
                 data-whitespace="nowrap"

                 data-type="shape"
                 data-basealign="slide"
                 data-responsive_offset="off"
                 data-responsive="off"
                 data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"speed":5000,"to":"opacity:0;","ease":"Power4.easeInOut"}]'
                 data-textAlign="['left','left','left','left']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[0,0,0,0]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[0,0,0,0]"

                 style="z-index: 5;background-color:rgba(0, 0, 0, 0.3);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

            <!-- LAYER NR. 2 -->
            <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                 id="slide-2800-layer-1"
                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-100','-100']"
                 data-fontsize="['37','34','25','30']"
                 data-lineheight="['37','34','25','30']"
                 data-width="none"
                 data-height="none"
                 data-whitespace="nowrap"

                 data-type="text"
                 data-responsive_offset="on"

                 data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                 data-textAlign="['center','center','center','center']"
                 data-paddingtop="[20,20,20,20]"
                 data-paddingright="[40,40,40,40]"
                 data-paddingbottom="[20,20,20,20]"
                 data-paddingleft="[40,40,40,40]"

                 style="z-index: 6; white-space: nowrap; color: rgba(255, 255, 255, 1.00);border-top: 2px solid #fff; border-bottom: 2px solid #fff;">
                 <i class="g-hidden-sm-down g-font-size-20 g-pos-rel g-top-minus-5 mr-2 icon-media-119 u-line-icon-pro"></i>
                 BABAI IRRIGATION PROJECT
                 <i class="g-hidden-sm-down g-font-size-20 g-pos-rel g-top-minus-5 ml-2 icon-media-119 u-line-icon-pro"></i>
            </div>

            <!-- LAYER NR. 4 -->
            <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                 id="slide-2800-layer-2"
                 data-x="['center','center','center','center']" data-hoffset="['-10','-10','-10','-10']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['50','50','50','50']"
                 data-fontsize="['27','27','25','30']"
                 data-lineheight="['40','40','35','40']"
                 data-width="none"
                 data-height="none"
                 data-whitespace="nowrap"

                 data-type="text"
                 data-responsive_offset="on"

                 data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                 data-textAlign="['center','center','center','center']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[0,0,0,0]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[0,0,0,0]"

                 style="z-index: 8; white-space: nowrap; font-weight: 400; color: rgba(255, 255, 255, 1.00);border-width:0px;">
                Largest Irrigation Project.<br> Irriagtion in Babai.
            </div>

            <!-- LAYER NR. 3 -->
            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-4"
                 id="slide-2800-layer-3"
                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['150','150','150','150']"
                 data-width="100"
                 data-height="1"
                 data-whitespace="nowrap"

                 data-type="shape"
                 data-responsive_offset="on"

                 data-frames='[{"from":"y:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                 data-textAlign="['left','left','left','left']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[0,0,0,0]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[0,0,0,0]"

                 style="z-index: 7;background-color:rgba(255, 255, 255, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

            <!-- LAYER NR. 5 -->
            <div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-4"
                 id="slide-2800-layer-4"
                 data-x="['right','right','center','center']" data-hoffset="['630','525','-105','100']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['240','240','240','240']"
                 data-width="none"
                 data-height="none"
                 data-whitespace="nowrap"

                 data-type="button"
                 data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                 data-responsive_offset="on"

                 data-frames='[{"from":"x:-50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'
                 data-textAlign="['left','left','left','left']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[50,50,50,50]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[50,50,50,50]"

                 style="z-index: 9; white-space: nowrap; font-size: 15px; line-height: 46px; color: rgba(255, 255, 255, 1.00);background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 1);border-style:solid;border-width:2px;border-radius:30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Learn More
            </div>

            <!-- LAYER NR. 7 -->
            <div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-4"
                 href="#portfolio-section" target="_self" id="slide-2800-layer-5"
                 data-x="['left','left','center','center']" data-hoffset="['630','525','105','100']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['240','240','240','240']"
                 data-width="none"
                 data-height="none"
                 data-whitespace="nowrap"

                 data-type="button"
                 data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                 data-responsive_offset="on"

                 data-frames='[{"from":"x:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'
                 data-textAlign="['left','left','left','left']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[50,50,50,50]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[50,50,50,50]"

                 style="z-index: 9; white-space: nowrap; font-size: 15px; line-height: 46px; color: rgba(255, 255, 255, 1.00);background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 1);border-style:solid;border-width:2px;border-radius:30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Our Work
            </div>
          </li>
          <li data-index="rs-2801" data-transition="slideup" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="/img2.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
            <!-- MAIN IMAGE -->
            <img src="/img2.jpg" alt="" data-bgposition="center center" data-bgfit="cover" class="rev-slidebg">
            <!-- LAYERS -->

            <!-- LAYER NR. 1 -->
            <div class="tp-caption tp-shape tp-shapewrapper "
                 id="slide-2801-layer-7"
                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                 data-width="full"
                 data-height="full"
                 data-whitespace="nowrap"

                 data-type="shape"
                 data-basealign="slide"
                 data-responsive_offset="off"
                 data-responsive="off"
                 data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"speed":5000,"to":"opacity:0;","ease":"Power4.easeInOut"}]'
                 data-textAlign="['left','left','left','left']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[0,0,0,0]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[0,0,0,0]"

                 style="z-index: 5;background-color:rgba(0, 0, 0, 0.3);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

            <!-- LAYER NR. 2 -->
            <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                 id="slide-2801-layer-1"
                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-100','-100']"
                 data-fontsize="['37','34','25','30']"
                 data-lineheight="['37','34','25','30']"
                 data-width="none"
                 data-height="none"
                 data-whitespace="nowrap"

                 data-type="text"
                 data-responsive_offset="on"

                 data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                 data-textAlign="['center','center','center','center']"
                 data-paddingtop="[20,20,20,20]"
                 data-paddingright="[40,40,40,40]"
                 data-paddingbottom="[20,20,20,20]"
                 data-paddingleft="[40,40,40,40]"

                 style="z-index: 6; white-space: nowrap; color: rgba(255, 255, 255, 1.00);border-top: 2px solid #fff; border-bottom: 2px solid #fff;">
                 <i class="g-hidden-sm-down g-font-size-20 g-pos-rel g-top-minus-5 mr-2 icon-media-119 u-line-icon-pro"></i>
                 MAIN CANAL
                 <i class="g-hidden-sm-down g-font-size-20 g-pos-rel g-top-minus-5 ml-2 icon-media-119 u-line-icon-pro"></i>
            </div>

            <!-- LAYER NR. 4 -->
            <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                 id="slide-2801-layer-2"
                 data-x="['center','center','center','center']" data-hoffset="['-10','-10','-10','-10']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['50','50','50','50']"
                 data-fontsize="['27','27','25','30']"
                 data-lineheight="['40','40','35','40']"
                 data-width="none"
                 data-height="none"
                 data-whitespace="nowrap"

                 data-type="text"
                 data-responsive_offset="on"

                 data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                 data-textAlign="['center','center','center','center']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[0,0,0,0]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[0,0,0,0]"

                 style="z-index: 8; white-space: nowrap; font-weight: 400; color: rgba(255, 255, 255, 1.00);border-width:0px;">
                Main canal stretches 1.2 KM.
            </div>

            <!-- LAYER NR. 3 -->
            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-4"
                 id="slide-2801-layer-3"
                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['150','150','150','150']"
                 data-width="100"
                 data-height="1"
                 data-whitespace="nowrap"

                 data-type="shape"
                 data-responsive_offset="on"

                 data-frames='[{"from":"y:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                 data-textAlign="['left','left','left','left']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[0,0,0,0]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[0,0,0,0]"

                 style="z-index: 7;background-color:rgba(255, 255, 255, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

            <!-- LAYER NR. 5 -->
            <div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-4"
                 id="slide-2801-layer-4"
                 data-x="['right','right','center','center']" data-hoffset="['630','525','-105','100']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['240','240','240','240']"
                 data-width="none"
                 data-height="none"
                 data-whitespace="nowrap"

                 data-type="button"
                 data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                 data-responsive_offset="on"

                 data-frames='[{"from":"x:-50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'
                 data-textAlign="['left','left','left','left']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[50,50,50,50]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[50,50,50,50]"

                 style="z-index: 9; white-space: nowrap; font-size: 15px; line-height: 46px; color: rgba(255, 255, 255, 1.00);background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 1);border-style:solid;border-width:2px;border-radius:30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Learn More
            </div>

            <!-- LAYER NR. 7 -->
            <div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-4"
                 href="#portfolio-section" target="_self" id="slide-2801-layer-5"
                 data-x="['left','left','center','center']" data-hoffset="['630','525','105','100']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['240','240','240','240']"
                 data-width="none"
                 data-height="none"
                 data-whitespace="nowrap"

                 data-type="button"
                 data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                 data-responsive_offset="on"

                 data-frames='[{"from":"x:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'
                 data-textAlign="['left','left','left','left']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[50,50,50,50]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[50,50,50,50]"

                 style="z-index: 9; white-space: nowrap; font-size: 15px; line-height: 46px; color: rgba(255, 255, 255, 1.00);background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 1);border-style:solid;border-width:2px;border-radius:30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Our Work
            </div>
          </li>
          <li data-index="rs-2802" data-transition="slidedown" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="/img3.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
            <!-- MAIN IMAGE -->
            <img src="/img3.jpg" alt="" data-bgposition="center center" data-bgfit="cover" class="rev-slidebg">
            <!-- LAYERS -->

            <!-- LAYER NR. 1 -->
            <div class="tp-caption tp-shape tp-shapewrapper "
                 id="slide-2802-layer-7"
                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                 data-width="full"
                 data-height="full"
                 data-whitespace="nowrap"

                 data-type="shape"
                 data-basealign="slide"
                 data-responsive_offset="off"
                 data-responsive="off"
                 data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"speed":5000,"to":"opacity:0;","ease":"Power4.easeInOut"}]'
                 data-textAlign="['left','left','left','left']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[0,0,0,0]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[0,0,0,0]"

                 style="z-index: 5;background-color:rgba(0, 0, 0, 0.3);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

            <!-- LAYER NR. 2 -->
            <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                 id="slide-2802-layer-1"
                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-100','-100']"
                 data-fontsize="['37','34','25','30']"
                 data-lineheight="['37','34','25','30']"
                 data-width="none"
                 data-height="none"
                 data-whitespace="nowrap"

                 data-type="text"
                 data-responsive_offset="on"

                 data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                 data-textAlign="['center','center','center','center']"
                 data-paddingtop="[20,20,20,20]"
                 data-paddingright="[40,40,40,40]"
                 data-paddingbottom="[20,20,20,20]"
                 data-paddingleft="[40,40,40,40]"

                 style="z-index: 6; white-space: nowrap; color: rgba(255, 255, 255, 1.00);border-top: 2px solid #fff; border-bottom: 2px solid #fff;">
                 <i class="g-hidden-sm-down g-font-size-20 g-pos-rel g-top-minus-5 mr-2 icon-media-119 u-line-icon-pro"></i>
                 COMMAND AREA
                 <i class="g-hidden-sm-down g-font-size-20 g-pos-rel g-top-minus-5 ml-2 icon-media-119 u-line-icon-pro"></i>
            </div>

            <!-- LAYER NR. 4 -->
            <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                 id="slide-2802-layer-2"
                 data-x="['center','center','center','center']" data-hoffset="['-10','-10','-10','-10']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['50','50','50','50']"
                 data-fontsize="['27','27','25','30']"
                 data-lineheight="['40','40','35','40']"
                 data-width="none"
                 data-height="none"
                 data-whitespace="nowrap"

                 data-type="text"
                 data-responsive_offset="on"

                 data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                 data-textAlign="['center','center','center','center']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[0,0,0,0]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[0,0,0,0]"

                 style="z-index: 8; white-space: nowrap; font-weight: 400; color: rgba(255, 255, 255, 1.00);border-width:0px;">
                 Creative freedom matters user experience.<br>We minimize the gap between technology and its audience.
            </div>

            <!-- LAYER NR. 3 -->
            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-4"
                 id="slide-2802-layer-3"
                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['150','150','150','150']"
                 data-width="100"
                 data-height="1"
                 data-whitespace="nowrap"

                 data-type="shape"
                 data-responsive_offset="on"

                 data-frames='[{"from":"y:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                 data-textAlign="['left','left','left','left']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[0,0,0,0]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[0,0,0,0]"

                 style="z-index: 7;background-color:rgba(255, 255, 255, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

            <!-- LAYER NR. 5 -->
            <div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-4"
                 id="slide-2802-layer-4"
                 data-x="['right','right','center','center']" data-hoffset="['630','525','-105','100']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['240','240','240','240']"
                 data-width="none"
                 data-height="none"
                 data-whitespace="nowrap"

                 data-type="button"
                 data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                 data-responsive_offset="on"

                 data-frames='[{"from":"x:-50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'
                 data-textAlign="['left','left','left','left']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[50,50,50,50]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[50,50,50,50]"

                 style="z-index: 9; white-space: nowrap; font-size: 15px; line-height: 46px; color: rgba(255, 255, 255, 1.00);background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 1);border-style:solid;border-width:2px;border-radius:30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Learn More
            </div>

            <!-- LAYER NR. 7 -->
            <div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-4"
                 href="#portfolio-section" target="_self" id="slide-2802-layer-5"
                 data-x="['left','left','center','center']" data-hoffset="['630','525','105','100']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['240','240','240','240']"
                 data-width="none"
                 data-height="none"
                 data-whitespace="nowrap"

                 data-type="button"
                 data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                 data-responsive_offset="on"

                 data-frames='[{"from":"x:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'
                 data-textAlign="['left','left','left','left']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[50,50,50,50]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[50,50,50,50]"

                 style="z-index: 9; white-space: nowrap; font-size: 15px; line-height: 46px; color: rgba(255, 255, 255, 1.00);background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 1);border-style:solid;border-width:2px;border-radius:30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Our Work
            </div>
          </li>
        </ul>
        <div class="tp-bannertimer" style="height: 2px; background-color: rgba(255, 255, 255, .5);"></div> </div>
      </div>
    </div>
  </div>
 <!-- END REVOLUTION SLIDER -->

      <!-- Promo Block -->
      {{-- <section class="g-py-50">
        <div class="container">
          <!-- News Section -->
          <div class="row no-gutters">
              @isset($newsList[0])
                <div class="col-lg-6 g-pr-1--lg g-mb-30 g-mb-2--lg">
                  <!-- Article -->
                  <article class="u-block-hover">
                    <figure class="u-shadow-v25 g-bg-cover g-bg-white-gradient-opacity-v1--after">
                      <img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="/news_images/{{ $newsList[0]->image }}" alt="{{ $newsList[0]->title }}">
                    </figure>
    
                
    
                    <div class="g-pos-abs g-bottom-30 g-left-30 g-right-30">
                      <small class="g-color-white">
                        <i class="icon-clock g-pos-rel g-top-1 g-mr-2"></i> {{ date('j M',strtotime($newsList[0]->published_date)) }}
                      </small>
    
                      <h3 class="h4 g-my-10">
                        <a class="g-color-white g-color-white--hover"  href="/news/{{ $newsList[0]->slug }}">{{ $newsList[0]->title }}</a>
                      </h3>
    
                    </div>
                  </article>
                  <!-- End Article -->
                </div>
              @endisset

            <div class="col-lg-6 g-pl-1--lg g-mb-30 g-mb-2--lg">
              <!-- Article -->
              <article class="u-block-hover">
                <figure class="u-shadow-v25 g-bg-cover g-bg-white-gradient-opacity-v1--after">
                  <img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="/img5.jpg" alt="Image Description">
                </figure>

                <span class="g-hidden-xs-down u-icon-v3 u-icon-size--sm g-font-size-13 g-bg-white rounded-circle g-pos-abs g-top-30 g-right-30">
                  <i class="fa fa-play g-left-2"></i>
                </span>

                <span class="g-hidden-xs-down g-pos-abs g-top-30 g-left-30">
                  <a class="btn btn-xs u-btn-red text-uppercase rounded-0" href="#!">Startup</a>
                </span>

                <div class="g-pos-abs g-bottom-30 g-left-30 g-right-30">
                  <small class="g-color-white">
                    <i class="icon-clock g-pos-rel g-top-1 g-mr-2"></i> July 22, 2016
                  </small>

                  <h3 class="h4 g-my-10">
                    <a class="g-color-white g-color-white--hover" href="#!">Why your customer support is very important? Learn the next 10 best tips.</a>
                  </h3>

                  <ul class="g-hidden-xs-down u-list-inline g-font-size-12 g-color-white">
                    <li class="list-inline-item">
                      <i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 127
                    </li>
                    <li class="list-inline-item">/</li>
                    <li class="list-inline-item">
                      <i class="icon-finance-206 u-line-icon-pro align-middle g-pos-rel g-top-1 g-mr-2"></i> 152
                    </li>
                    <li class="list-inline-item">/</li>
                    <li class="list-inline-item">
                      <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 32
                    </li>
                  </ul>
                </div>
              </article>
              <!-- End Article -->
            </div>

            <div class="col-lg-4 g-pr-1--lg g-mb-30 g-mb-0--lg">
              <!-- Article -->
              <article class="u-block-hover">
                <figure class="u-shadow-v25 u-bg-overlay g-bg-white-gradient-opacity-v1--after">
                  <img class="u-block-hover__main--zoom-v1 img-fluid w-100" src="/img6.jpg" alt="Image Description">
                </figure>

                <div class="w-100 text-center g-absolute-centered g-px-30">
                  <a class="btn btn-xs u-btn-cyan text-uppercase rounded-0" href="#!">Spa</a>
                  <h3 class="h4 g-mt-10">
                    <a class="g-color-white" href="#!">Be ready, fashion of the year is coming this year</a>
                  </h3>
                  <small class="g-color-white">
                    <i class="icon-clock g-pos-rel g-top-1 g-mr-2"></i> July 26, 2017
                  </small>
                </div>
              </article>
              <!-- End Article -->
            </div>

            <div class="col-lg-4 g-px-1--lg g-mb-30 g-mb-0--lg">
              <!-- Article -->
              <article class="u-block-hover">
                <figure class="u-shadow-v25 u-bg-overlay g-bg-white-gradient-opacity-v1--after">
                  <img class="u-block-hover__main--zoom-v1 img-fluid w-100" src="/img7.jpg" alt="Image Description">
                </figure>

                <div class="w-100 text-center g-absolute-centered g-px-30">
                  <a class="btn btn-xs u-btn-pink text-uppercase rounded-0" href="#!">Fashion</a>
                  <h3 class="h4 g-mt-10">
                    <a class="g-color-white" href="#!">Must be visited places in the USA - Florida Beaches</a>
                  </h3>
                  <small class="g-color-white">
                    <i class="icon-clock g-pos-rel g-top-1 g-mr-2"></i> July 18, 2017
                  </small>
                </div>
              </article>
              <!-- End Article -->
            </div>

            <div class="col-lg-4 g-pl-1--lg">
              <!-- Article -->
              <article class="u-block-hover">
                <figure class="u-shadow-v25 u-bg-overlay g-bg-white-gradient-opacity-v1--after">
                  <img class="u-block-hover__main--zoom-v1 img-fluid w-100" src="/img8.jpg" alt="Image Description">
                </figure>

                <div class="w-100 text-center g-absolute-centered g-px-30">
                  <a class="btn btn-xs u-btn-primary text-uppercase rounded-0" href="#!">Tech</a>
                  <h3 class="h4 g-mt-10">
                    <a class="g-color-white" href="#!">Why your next glass of juice will cost you more</a>
                  </h3>
                  <small class="g-color-white">
                    <i class="icon-clock g-pos-rel g-top-1 g-mr-2"></i> July 5, 2017
                  </small>
                </div>
              </article>
              <!-- End Article -->
            </div>
          </div>
          <!-- News Section -->
        </div>
      </section> --}}
      <!-- End Promo Block -->

    
      <section class="g-my-40 g-mx-100">
        <div class="row">

            <div class="col-sm-12 col-lg-8 col-md-8">
              <div class="row">
                <div class="col-md-6 g-px">
                  <div class="text-center g-brd-primary" style="border:1px solid; padding:1rem; border-radius:10px">
                      <h4 class="g-mb-20">Our Projects</h4>

                      <a href="/reports" class="btn btn-primary">View Projects.</a>
                  </div>
                </div>
                <div class="col-md-6">
                    <div class="text-center g-brd-primary" style="border:1px solid; padding:1rem; border-radius:10px">
                        <h4 class="g-mb-20">Project Report</h4>
  
                        <a href="/reports" class="btn btn-primary">View Report.</a>
                    </div>
                </div>

              </div>
                <hr>

                <div class="text-center">
                    <h4 class="g-mb-20">Notices</h4>
                </div>
                <ul class="list-unstyled">
                  @foreach($notices as $notice)
                    <li class="g-brd-around g-brd-gray-light-v4 g-pa-20 g-mb-minus-1">
                      <div class="media">
                        <div class="media-body">
                        <p class="m-0">{{$notice->title}}</p>
                          <span class="g-font-size-12 g-color-gray">{{$notice->published_date}}</span>
                        </div>
                      </div>
                    </li>
                    @endforeach
                  </ul>
                  <div class="text-center">
                      <a href="/notices" class="btn btn-primary">View All.</a>

                  </div>
                  
            </div>

              <div class="col-md-4 g-mt-10 text-center" >
                  <img src="/personnel_images/{{$personnels[0]->image}}" style="height:15rem" />
                  <br>
                  <div class="g-px-3 g-px-10--lg g-py-30">
                      <h3 class="h4 mb-1">{{$personnels[0]->name}}</h3>
                      <span class="d-block">{{$personnels[0]->designation}}r</span>
                      <span class="d-block mb-4">{{$personnels[0]->department}}</span>

                    <p class="mb-4"></p>
            
                    </div>


                    <img src="/personnel_images/{{$personnels[1]->image}}" style="height:15rem" />
                  <br>
                  <div class="g-px-3 g-px-10--lg g-py-30">
                      <h3 class="h4 mb-1">{{$personnels[1]->name}}</h3>
                      <span class="d-block">{{$personnels[1]->designation}}</span>
                      <span class="d-block mb-4">{{$personnels[0]->department}}</span>

                    <p class="mb-4"></p>
            
                    </div>


              </div>
        

        </div>
      </section>
        <!-- End Carousel -->
      <section class="g-py-50">
        <div class="g-mx-20">
          <div class="row">
            <div class="col-md-8 " >
              <div class="text-center">
                  <h4 class="g-mb-20">News Updates</h4>
              </div>
              <div class="row">
                @foreach($newsList as $news)
                <div class="col-lg-4 g-mb-30">
                  <!-- Article -->
                  <article class="u-block-hover">
                    <figure class="u-bg-overlay g-bg-black-opacity-0_2--after">
                    <img class="img-fluid u-block-hover__main--zoom-v1" src="/news_images/{{$news->image}}" alt="Image Description">
                    </figure>
              
                    <div class="w-100 text-center g-absolute-centered g-px-20">
                      <h3 class="h4 g-font-weight-600 g-mt-10">
                        <a class="g-color-white" href="/news/{{ $news->slug }}">{{$news->title}}</a>
                      </h3>
                      <small class="g-color-white">
                        <i class="icon-clock g-pos-rel g-top-1 g-mr-2"></i>
                        {{$news->published_date}}
                      </small>
                    </div>
                  </article>
                  <!-- End Article -->
                </div>
                @endforeach
              </div>
              <div class="text-center">
                  <a href="/news" class="btn btn-primary">View All.</a>

              </div>
            </div>
            <div class="col-md-4">
                <div class="text-center">
                    <h4 class="g-mb-20">Gallery</h4>
                </div>
                <div class="row">
                  @foreach($galleries as $image)
                  <div class="col-md-4 g-mb-30">
                    <a class="js-fancybox d-block u-block-hover" href="javascript:;" data-fancybox="lightbox-gallery--03" data-src="/frontend-assets/main-assets/assets/img-temp/740x380/img1.jpg" data-speed="350" data-caption="Lightbox Gallery">
                    <img class="img-fluid u-block-hover__main--mover-right" src="/gallery/{{$image->image}}" alt="Image Description">
                    </a>
                  </div>
                  @endforeach
                </div>
                <div class="text-center">
                    <a href="/albums" class="btn btn-primary">View All.</a>
  
                </div>

            </div>
          </div>
        </div>
      </section>
 
@endsection


@section('js')

<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>

<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution-addons/polyfold/js/revolution.addon.polyfold.min.js"></script>

<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>

<script>
  $(document).on('ready', function () {
    // initialization of carousel
    $.HSCore.components.HSCarousel.init('.js-carousel');

    // initialization of masonry
    $('.masonry-grid').imagesLoaded().then(function () {
      $('.masonry-grid').masonry({
        columnWidth: '.masonry-grid-sizer',
        itemSelector: '.masonry-grid-item',
        percentPosition: true
      });
    });

    // Header
    $.HSCore.components.HSHeader.init($('#js-header'));
    $.HSCore.helpers.HSHamburgers.init('.hamburger');

    // Initialization of HSMegaMenu plugin
    $('.js-mega-menu').HSMegaMenu({
      event: 'hover',
      pageContainer: $('.container'),
      breakpoint: 991
    });
  });

  var tpj = jQuery;

var revapi1014;
tpj(document).ready(function () {
  if (tpj("#rev_slider_1014_1").revolution == undefined) {
    revslider_showDoubleJqueryError("#rev_slider_1014_1");
  } else {
    revapi1014 = tpj("#rev_slider_1014_1").show().revolution({
      sliderType: "standard",
      jsFileLocation: "revolution/js/",
      sliderLayout: "fullscreen",
      dottedOverlay: "none",
      delay: 9000,
      navigation: {
        keyboardNavigation: "off",
        keyboard_direction: "horizontal",
        mouseScrollNavigation: "off",
        mouseScrollReverse: "default",
        onHoverStop: "off",
        touch: {
          touchenabled: "on",
          swipe_threshold: 75,
          swipe_min_touches: 1,
          swipe_direction: "horizontal",
          drag_block_vertical: false
        },
        arrows: {
          style: "hermes",
          enable: true,
          hide_onmobile: true,
          hide_under: 768,
          hide_onleave: false,
          tmp: '<div class="tp-arr-allwrapper"><div class="tp-arr-imgholder"></div><div class="tp-arr-titleholder">Pocket Consultancy</div></div>',
          left: {
            h_align: "left",
            v_align: "center",
            h_offset: 0,
            v_offset: 0
          },
          right: {
            h_align: "right",
            v_align: "center",
            h_offset: 0,
            v_offset: 0
          }
        }
      },
      responsiveLevels:[1240,1024,778,778],
      gridwidth:[1240,1024,778,480],
      gridheight:[600,500,400,300],
      lazyType: 'smart',
      scrolleffect: {
        fade: "on",
        grayscale: "on",
        on_slidebg: "on",
        on_parallax_layers: "on",
        direction: "top",
        multiplicator_layers: "1.4",
        tilt: "10",
        disable_on_mobile: "off",
      },
      parallax: {
        type: "scroll",
        origo: "slidercenter",
        speed: 400,
        levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 51, 55],
      },
      shadow: 0,
      spinner: "off",
      stopLoop: 'off',
      stopAfterLoops: -1,
      stopAtSlide: -1,
      shuffle: "off",
      autoHeight: "off",
      fullScreenAutoWidth: "off",
      fullScreenAlignForce: "off",
      fullScreenOffsetContainer: "",
      fullScreenOffset: "0px",
      hideThumbsOnMobile: "off",
      hideSliderAtLimit: 0,
      hideCaptionAtLimit: 0,
      hideAllCaptionAtLilmit: 0,
      debugMode: false,
      fallbacks: {
        simplifyAll: "off",
        nextSlideOnWindowFocus: "off",
        disableFocusListener: false,
      }
    });
  }

RsTypewriterAddOn(tpj, revapi1014);
});


</script>
@endsection