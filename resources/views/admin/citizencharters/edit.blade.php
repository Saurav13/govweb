@extends('layouts.admin')

@section('body')

    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Edit Citizen Charter</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" action="{{ route('citizencharters.update',$citizencharter->id) }}"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH" >

                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Service Title</label>
                                        <input class="form-control{{ $errors->has('en_service_title') ? ' border-danger' : '' }}" id="en_service_title" type="text" class="form-control" name="en_service_title" value="{{ $citizencharter->en_service_title }}" required>

                                        @if ($errors->has('en_service_title'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_service_title') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">सेवा शीर्षक</label>
                                        <input class="form-control{{ $errors->has('ne_service_title') ? ' border-danger' : '' }}" id="ne_service_title" type="text" class="form-control" name="ne_service_title" value="{{ $citizencharter->ne_service_title }}">

                                        @if ($errors->has('ne_service_title'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_service_title') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                                
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Service fee</label>
                                        @if ($errors->has('en_service_fee'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_service_fee') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('en_service_fee') ? ' border-danger' : '' }}" id="en_service_fee" type="text" class="form-control" name="en_service_fee" value="{{ $citizencharter->en_service_fee }}" required>
                                
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>सेवा शुल्क</label>
                                        @if ($errors->has('ne_service_fee'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_service_fee') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('ne_service_fee') ? ' border-danger' : '' }}" id="ne_service_fee" type="text" class="form-control" name="ne_service_fee" value="{{ $citizencharter->ne_service_fee }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Service time</label>
                                        @if ($errors->has('en_service_time'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_service_time') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('en_service_time') ? ' border-danger' : '' }}" id="en_service_time" type="text" class="form-control" name="en_service_time" value="{{ $citizencharter->en_service_time }}" required>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>सेवा समय</label>
                                        @if ($errors->has('ne_service_time'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_service_time') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('ne_service_time') ? ' border-danger' : '' }}" id="ne_service_time" type="text" class="form-control" name="ne_service_time" value="{{ $citizencharter->ne_service_time }}">

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Responsible Department</label>
                                        @if ($errors->has('en_responsible_dept'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_responsible_dept') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('en_responsible_dept') ? ' border-danger' : '' }}" id="en_responsible_dept" type="text" class="form-control" name="en_responsible_dept" value="{{ $citizencharter->en_responsible_dept }}" required>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>विभाग</label>
                                        @if ($errors->has('ne_responsible_dept'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_responsible_dept') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('ne_responsible_dept') ? ' border-danger' : '' }}" id="ne_responsible_dept" type="text" class="form-control" name="ne_responsible_dept" value="{{ $citizencharter->ne_responsible_dept }}">

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Remarks</label>
                                        @if ($errors->has('en_remarks'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_remarks') }}</strong>
                                            </div>
                                        @endif
                                        <textarea class="form-control{{ $errors->has('en_remarks') ? ' border-danger' : '' }}" id="en_remarks" type="text" class="form-control" name="en_remarks" required>{{ $citizencharter->en_remarks }}</textarea>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>रेमर्क्स</label>
                                        @if ($errors->has('ne_remarks'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_remarks') }}</strong>
                                            </div>
                                        @endif
                                        <textarea class="form-control{{ $errors->has('ne_remarks') ? ' border-danger' : '' }}" id="ne_remarks" type="text" class="form-control" name="ne_remarks">{{ $citizencharter->ne_remarks }}</textarea>

                                    </div>
                                </div>
                            </div>

                        </div>
    

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection