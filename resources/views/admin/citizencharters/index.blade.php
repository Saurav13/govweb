@extends('layouts.admin')

@section('body')
   
   <div class="content-header row">
   </div>

   <div class="content-body">
       <div class="card">
           <div class="card-header">
               <h4 class="card-title"><a data-action="collapse">Add New Citizen Charter</a></h4>
               <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                       <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                   </ul>
               </div>
           </div>
           <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
               <div class="card-block card-dashboard">
                   <form class="form" method="POST" id="AddBlogForm" action="{{ route('citizencharters.store') }}" enctype="multipart/form-data">
                       {{ csrf_field() }}
                       <div class="form-body">
                            <h4 class="form-section"><i class="icon-eye6"></i> Citizen Charter</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Service Title</label>
                                        <input class="form-control{{ $errors->has('en_service_title') ? ' border-danger' : '' }}" id="en_service_title" type="text" class="form-control" name="en_service_title" value="{{ old('en_service_title') }}" required>

                                        @if ($errors->has('en_service_title'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_service_title') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">सेवा शीर्षक</label>
                                        <input class="form-control{{ $errors->has('ne_service_title') ? ' border-danger' : '' }}" id="ne_service_title" type="text" class="form-control" name="ne_service_title" value="{{ old('ne_service_title') }}">

                                        @if ($errors->has('ne_service_title'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_service_title') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                              
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Service fee</label>
                                        @if ($errors->has('en_service_fee'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_service_fee') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('en_service_fee') ? ' border-danger' : '' }}" id="en_service_fee" type="text" class="form-control" name="en_service_fee" value="{{ old('en_service_fee') }}" required>
                                
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>सेवा शुल्क</label>
                                        @if ($errors->has('ne_service_fee'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_service_fee') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('ne_service_fee') ? ' border-danger' : '' }}" id="ne_service_fee" type="text" class="form-control" name="ne_service_fee" value="{{ old('ne_service_fee') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Service time</label>
                                        @if ($errors->has('en_service_time'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_service_time') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('en_service_time') ? ' border-danger' : '' }}" id="en_service_time" type="text" class="form-control" name="en_service_time" value="{{ old('en_service_time') }}" required>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>सेवा समय</label>
                                        @if ($errors->has('ne_service_time'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_service_time') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('ne_service_time') ? ' border-danger' : '' }}" id="ne_service_time" type="text" class="form-control" name="ne_service_time" value="{{ old('ne_service_time') }}">

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Responsible Department</label>
                                        @if ($errors->has('en_responsible_dept'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_responsible_dept') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('en_responsible_dept') ? ' border-danger' : '' }}" id="en_responsible_dept" type="text" class="form-control" name="en_responsible_dept" value="{{ old('en_responsible_dept') }}" required>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>विभाग</label>
                                        @if ($errors->has('ne_responsible_dept'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_responsible_dept') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('ne_responsible_dept') ? ' border-danger' : '' }}" id="ne_responsible_dept" type="text" class="form-control" name="ne_responsible_dept" value="{{ old('ne_responsible_dept') }}">

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Remarks</label>
                                        @if ($errors->has('en_remarks'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_remarks') }}</strong>
                                            </div>
                                        @endif
                                        <textarea class="form-control{{ $errors->has('en_remarks') ? ' border-danger' : '' }}" id="en_remarks" type="text" class="form-control" name="en_remarks" required>{{ old('en_remarks') }}</textarea>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>रेमर्क्स</label>
                                        @if ($errors->has('ne_remarks'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_remarks') }}</strong>
                                            </div>
                                        @endif
                                        <textarea class="form-control{{ $errors->has('ne_remarks') ? ' border-danger' : '' }}" id="ne_remarks" type="text" class="form-control" name="ne_remarks">{{ old('ne_remarks') }}</textarea>

                                    </div>
                                </div>
                            </div>

                       </div>

                       <div class="form-actions right">
                           <button type="submit" class="btn btn-primary">
                               <i class="icon-check2"></i> Save
                           </button>
                       </div>
                   </form>
               </div>
           </div>
       </div>

       <div class="card">
           <div class="card-header">
               <h4 class="card-title">Citizen Charters</h4>
               <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                       <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                   </ul>
               </div>
           </div>
           <div class="card-body collapse in">
               <div class="card-block card-dashboard">
                   <div class="table-responsive">
                       <table class="table">
                           <thead>
                               <tr>
                                   <th>#</th>
                                   <th>Service Title</th>
                                   <th>Service Fee</th>
                                   <th>Service Time</th>
                                   <th>Department</th>
                                   <th width="20%">Actions</th>
                               </tr>
                           </thead>
                           <tbody>
                               @foreach($citizencharters as $citizencharter)
                               <tr>
                                   <td>{{ $loop->iteration + (($citizencharters->currentPage()-1) * $citizencharters->perPage()) }}</td>
                                   <td>{{ $citizencharter->en_service_title }}</td>
                                   <td>{{ $citizencharter->en_service_fee }}</td>
                                   <td>{{ $citizencharter->en_service_time }}</td>
                                   <td>{{ $citizencharter->en_responsible_dept }}</td>
                                   <td>
                                        <a class="btn btn-outline-primary" title="View Details" href="{{ route('citizencharters.show',$citizencharter->id) }}"><i class="icon-eye"></i></a>

                                        <a class="btn btn-outline-warning" title="Update Details" href="{{ route('citizencharters.edit',$citizencharter->id) }}"><i class="icon-edit"></i></a>
                                        
                                        <form action="{{ route('citizencharters.destroy',$citizencharter->id) }}" method="POST" style="display:inline">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE" >
                                            <button id='deleteCitizen Charter{{ $citizencharter->id }}' type="button" class="btn btn-outline-danger"><i class="icon-trash-o"></i></button>
                                        </form>
                                   </td>
                               </tr>
                               @endforeach
                           </tbody>
                       </table>


                       <div class="text-xs-center mb-3">
                           <nav aria-label="Page navigation">
                               {{ $citizencharters->links() }}
                           </nav>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>

   
@endsection