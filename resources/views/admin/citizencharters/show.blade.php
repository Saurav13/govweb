@extends('layouts.admin')

@section('body')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Citizen Charter #{{ $citizencharter->id }}</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('citizencharters.index') }}">Back</a>
                    <a class="btn btn-warning btn-min-width mr-1 mb-1 " href="{{ route('citizencharters.edit',$citizencharter->id) }}">Edit</a>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section class="card">
            <div class="card-body collapse in">
                <div class="card-block">
                    
                    <div id="invoice-template" class="card-block">
                        
                        <div id="invoice-items-details" class="pt-2">
                            <div class="col-md-12 col-sm-12 text-xs-center text-md-left">
                                <table class="table">
                                    <tbody style="border-top:0">
                                        <tr >
                                            <td  style="    border-top: 0;width:30%"><strong>Service Title: </strong></td>
                                            <td  style="    border-top: 0;">{{ $citizencharter->en_service_title }}</td>
                                        </tr>
                                        <tr >
                                            <td  style="    border-top: 0;"><strong>सेवा शीर्षक: </strong></td>
                                            <td  style="    border-top: 0;">{{ $citizencharter->ne_service_title }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Service fee: </strong></td>
                                            <td>{{ $citizencharter->en_service_fee }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>सेवा शुल्क: </strong></td>
                                            <td>{{ $citizencharter->ne_service_fee }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Service time: </strong></td>
                                            <td>{{ $citizencharter->en_service_time }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>सेवा समय: </strong></td>
                                            <td>{{ $citizencharter->ne_service_time }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Responsible Department: </strong></td>
                                            <td>{{ $citizencharter->en_responsible_dept }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>विभाग: </strong></td>
                                            <td>{{ $citizencharter->ne_responsible_dept }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Remarks: </strong></td>
                                            <td>{{ $citizencharter->en_remarks }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>रेमर्क्स: </strong></td>
                                            <td>{{ $citizencharter->ne_remarks }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    
@endsection
