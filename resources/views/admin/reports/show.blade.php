@extends('layouts.admin')

@section('body')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Report #{{ $report->id }}</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('reports.index') }}">Back</a>
                    <a class="btn btn-warning btn-min-width mr-1 mb-1 " href="{{ route('reports.edit',$report->id) }}">Edit</a>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section class="card">
            <div class="card-body collapse in">
                <div class="card-block">
                    
                    <div id="invoice-template" class="card-block">
                        
                        <div id="invoice-items-details" class="pt-2">
                            <div class="col-md-12 col-sm-12 text-xs-center text-md-left">
                                <table class="table">
                                    <tbody style="border-top:0">
                                        <tr >
                                            <td  style="    border-top: 0;width:20%"><strong>Contract Id: </strong></td>
                                            <td  style="    border-top: 0;">{{ $report->contract_id }}</td>
                                        </tr>
                                        <tr >
                                            <td  style="    border-top: 0;"><strong>Project Title: </strong></td>
                                            <td  style="    border-top: 0;">{{ $report->project_title }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Details: </strong></td>
                                            <td>{{ $report->details }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Estimated Amount: </strong></td>
                                            <td>{{ $report->estimated_amount }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Contractor Name: </strong></td>
                                            <td>{{ $report->contractor_name }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Contract Date: </strong></td>
                                            <td>{{ $report->contract_date }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Contract Amount: </strong></td>
                                            <td>{{ $report->contract_amount }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Due Date Completion: </strong></td>
                                            <td>{{ $report->due_date_completion }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Bill Amount till Date: </strong></td>
                                            <td>{{ $report->bill_amount_till_date }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Payment Due: </strong></td>
                                            <td>{{ $report->payment_due }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Financial Progress: </strong></td>
                                            <td>{{ $report->financial_progress }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Attorney Holder Name: </strong></td>
                                            <td>{{ $report->attorney_holder_name }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Attorney Holder Telno: </strong></td>
                                            <td>{{ $report->attorney_holder_tel_no }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Mobilization Total: </strong></td>
                                            <td>{{ $report->mobilization_total }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Mobilization Due: </strong></td>
                                            <td>{{ $report->mobilization_due }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>PGB Amount: </strong></td>
                                            <td>{{ $report->PGB_amount }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>PGB Date: </strong></td>
                                            <td>{{ $report->PGB_date }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>APG Amount: </strong></td>
                                            <td>{{ $report->APG_amount }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>APG Date: </strong></td>
                                            <td>{{ $report->APG_date }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Remark Date: </strong></td>
                                            <td>{{ $report->remark_date }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Side SDE: </strong></td>
                                            <td>{{ $report->side_sdk }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Type 1: </strong></td>
                                            <td>{{ $report->type1 }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Type 2: </strong></td>
                                            <td>{{ $report->type2 }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

