@extends('layouts.admin')

@section('body')

    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Edit Report</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <form class="form" method="POST" action="{{ route('reports.update',$report->id) }}"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH" >

                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="contract_id">Contract Id</label>
                                        <input class="form-control{{ $errors->has('contract_id') ? ' border-danger' : '' }}" id="contract_id" type="text" class="form-control" name="contract_id" value="{{ $report->contract_id }}" required>
                            
                                        @if ($errors->has('contract_id'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('contract_id') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Name of Work</label>
                                <textarea class="form-control{{ $errors->has('details') ? ' border-danger' : '' }}"  name="details">{{ $report->details }}</textarea>

                                @if ($errors->has('details'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('details') }}</strong>
                                    </div>
                                @endif
                            </div>
                                
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Estimated Amount</label>
                                        @if ($errors->has('estimated_amount'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('estimated_amount') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('estimated_amount') ? ' border-danger' : '' }}" id="estimated_amount" type="text" class="form-control" name="estimated_amount" value="{{ $report->estimated_amount }}">
                                
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contractor Name</label>
                                        @if ($errors->has('contractor_name'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('contractor_name') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('contractor_name') ? ' border-danger' : '' }}" id="contractor_name" type="text" class="form-control" name="contractor_name" value="{{ $report->contractor_name }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract Date</label>
                                        @if ($errors->has('contract_date'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('contract_date') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('contract_date') ? ' border-danger' : '' }}" id="contract_date" type="text" class="form-control" name="contract_date" value="{{ $report->contract_date }}">

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract Amount</label>
                                        @if ($errors->has('contract_amount'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('contract_amount') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('contract_amount') ? ' border-danger' : '' }}" id="contract_amount" type="text" class="form-control" name="contract_amount" value="{{ $report->contract_amount }}">

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Due Date Completion</label>
                                        @if ($errors->has('due_date_completion'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('due_date_completion') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('due_date_completion') ? ' border-danger' : '' }}" id="due_date_completion" type="text" class="form-control" name="due_date_completion" value="{{ $report->due_date_completion }}">
                            
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Bill Amount Till Date</label>
                                        @if ($errors->has('bill_amount_till_date'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('bill_amount_till_date') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('bill_amount_till_date') ? ' border-danger' : '' }}" id="bill_amount_till_date" type="text" class="form-control" name="bill_amount_till_date" value="{{ $report->bill_amount_till_date }}">
                            
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Payment Due</label>
                                        @if ($errors->has('payment_due'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('payment_due') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('payment_due') ? ' border-danger' : '' }}" id="payment_due" type="text" class="form-control" name="payment_due" value="{{ $report->payment_due }}">
                            
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Financial Progress</label>
                                        @if ($errors->has('financial_progress'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('financial_progress') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('financial_progress') ? ' border-danger' : '' }}" id="financial_progress" type="text" class="form-control" name="financial_progress" value="{{ $report->financial_progress }}">
                            
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Attorney Holder Name</label>
                                        @if ($errors->has('attorney_holder_name'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('attorney_holder_name') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('attorney_holder_name') ? ' border-danger' : '' }}" id="attorney_holder_name" type="text" class="form-control" name="attorney_holder_name" value="{{ $report->attorney_holder_name }}">
                            
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Attorney Holder Tel_no</label>
                                        @if ($errors->has('attorney_holder_tel_no'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('attorney_holder_tel_no') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('attorney_holder_tel_no') ? ' border-danger' : '' }}" id="attorney_holder_tel_no" type="text" class="form-control" name="attorney_holder_tel_no" value="{{ $report->attorney_holder_tel_no }}">
                            
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mobilization Total</label>
                                        @if ($errors->has('mobilization_total'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('mobilization_total') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('mobilization_total') ? ' border-danger' : '' }}" id="mobilization_total" type="text" class="form-control" name="mobilization_total" value="{{ $report->mobilization_total }}">
                            
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mobilization Due</label>
                                        @if ($errors->has('mobilization_due'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('mobilization_due') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('mobilization_due') ? ' border-danger' : '' }}" id="mobilization_due" type="text" class="form-control" name="mobilization_due" value="{{ $report->mobilization_due }}">
                            
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>PGB amount</label>
                                        @if ($errors->has('PGB_amount'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('PGB_amount') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('PGB_amount') ? ' border-danger' : '' }}" id="PGB_amount" type="text" class="form-control" name="PGB_amount" value="{{ $report->PGB_amount }}">
                            
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>PGB Date</label>
                                        @if ($errors->has('PGB_date'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('PGB_date') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('PGB_date') ? ' border-danger' : '' }}" id="PGB_date" type="text" class="form-control" name="PGB_date" value="{{ $report->PGB_date }}">
                            
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>APG amount</label>
                                        @if ($errors->has('APG_amount'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('APG_amount') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('APG_amount') ? ' border-danger' : '' }}" id="APG_amount" type="text" class="form-control" name="APG_amount" value="{{ $report->APG_amount }}">
                            
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>APG Date</label>
                                        @if ($errors->has('APG_date'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('APG_date') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('APG_date') ? ' border-danger' : '' }}" id="APG_date" type="text" class="form-control" name="APG_date" value="{{ $report->APG_date }}">
                            
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Remark Date</label>
                                        @if ($errors->has('remark_date'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('remark_date') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('remark_date') ? ' border-danger' : '' }}" id="remark_date" type="text" class="form-control" name="remark_date" value="{{ $report->remark_date }}">
                            
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Side SDE</label>
                                        @if ($errors->has('side_sdk'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('side_sdk') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('side_sdk') ? ' border-danger' : '' }}" id="side_sdk" type="text" class="form-control" name="side_sdk" value="{{ $report->side_sdk }}">
                            
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="type1">Type1</label>
                                        <select class="form-control{{ $errors->has('type1') ? ' border-danger' : '' }}" id="type1" class="form-control" name="type1">
                                            <option {{ !$report->type1 ? 'selected' : '' }} disabled value="">Choose a type</option>
                                            <option value="Annual" {{ $report->type1 == 'Annual' ? 'selected' : '' }}>Annual</option>
                                            <option value="Quarterly" {{ $report->type1 == 'Quarterly' ? 'selected' : '' }}>Quarterly</option>
                                            <option value="Monthly" {{ $report->type1 == 'Monthly' ? 'selected' : '' }}>Monthly</option>
                                            <option value="Weekly" {{ $report->type1 == 'Weekly' ? 'selected' : '' }}>Weekly</option>
                                        </select>

                                        @if ($errors->has('type1'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('type1') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="type2">Type2</label>
                                        <select class="form-control{{ $errors->has('type2') ? ' border-danger' : '' }}" id="type2" class="form-control" name="type2">
                                            <option {{ !$report->type2 ? 'selected' : '' }} disabled value="">Choose a type</option>
                                            <option value="Physical" {{ $report->type2 == 'Physical' ? 'selected' : '' }}>Physical</option>
                                            <option value="Financial" {{ $report->type2 == 'Financial' ? 'selected' : '' }}>Financial</option>
                                        </select>

                                        @if ($errors->has('type2'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('type2') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
    

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon-check2"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection