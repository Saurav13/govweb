@extends('layouts.admin')

@section('body')
   
   <div class="content-header row">
   </div>
   <input name="image" type="file" id="upload" class="hidden" onchange="">

   <div class="content-body">
       <div class="card">
           <div class="card-header">
               <h4 class="card-title"><a data-action="collapse">Add New Info</a></h4>
               <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                       <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                   </ul>
               </div>
           </div>
           <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
               <div class="card-block card-dashboard">
                   <form class="form" method="POST" id="AddInfoForm" action="{{ route('infos.store') }}" enctype="multipart/form-data">
                       {{ csrf_field() }}
                       <div class="form-body">
                            <h4 class="form-section"><i class="icon-eye6"></i> Info</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input class="form-control{{ $errors->has('en_title') ? ' border-danger' : '' }}" id="en_title" type="text" class="form-control" name="en_title" value="{{ old('en_title') }}" required>

                                        @if ($errors->has('en_title'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_title') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="title">टाइटल
                                        </label>
                                        <input class="form-control{{ $errors->has('ne_title') ? ' border-danger' : '' }}" id="ne_title" type="text" class="form-control" name="ne_title" value="{{ old('ne_title') }}">

                                        @if ($errors->has('ne_title'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_title') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                          
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Details</label>
                                        @if ($errors->has('en_description'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_description') }}</strong>
                                            </div>
                                        @endif
                                        <textarea cols="50" rows="20" class="content form-control{{ $errors->has('en_description') ? ' border-danger' : '' }}" id="en_content" name="en_description">{{ old('en_description') }}</textarea>
                                
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>डिटेल</label>
                                        @if ($errors->has('ne_description'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_description') }}</strong>
                                            </div>
                                        @endif
                                        <textarea cols="50" rows="20" class="content form-control{{ $errors->has('ne_description') ? ' border-danger' : '' }}" id="ne_content" name="ne_description">{{ old('ne_description') }}</textarea>
                                
                                    </div>
                                </div>
                            </div>
                       </div>

                       <div class="form-actions right">
                           <button type="submit" class="btn btn-primary">
                               <i class="icon-check2"></i> Save
                           </button>
                       </div>
                   </form>
               </div>
           </div>
       </div>

       <div class="card">
           <div class="card-header">
               <h4 class="card-title">Info Posts</h4>
               <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                       <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                   </ul>
               </div>
           </div>
           <div class="card-body collapse in">
               <div class="card-block card-dashboard">
                   <div class="table-responsive">
                       <table class="table">
                           <thead>
                               <tr>
                                   <th>#</th>
                                   <th>Title</th>
                                   <th width="20%">Actions</th>
                               </tr>
                           </thead>
                           <tbody>
                               @foreach($infos as $info)
                               <tr>
                                   <td>{{ $loop->iteration + (($infos->currentPage()-1) * $infos->perPage()) }}</td>
                                   <td>{{ $info->en_title }}</td>
                                    <td>
                                        <a class="btn btn-outline-primary" title="View Details" href="{{ route('infos.show',$info->id) }}"><i class="icon-eye"></i></a>

                                       <a class="btn btn-outline-warning" title="Update Details" href="{{ route('infos.edit',$info->id) }}"><i class="icon-edit"></i></a>
                                       
                                       <form action="{{ route('infos.destroy',$info->id) }}" method="POST" style="display:inline">
                                           {{ csrf_field() }}
                                           <input type="hidden" name="_method" value="DELETE" >
                                           <button id='deleteInfo{{ $info->id }}' type="button" class="btn btn-outline-danger"><i class="icon-trash-o"></i></button>
                                       </form>
                                   </td>
                               </tr>
                               @endforeach
                           </tbody>
                       </table>


                       <div class="text-xs-center mb-3">
                           <nav aria-label="Page navigation">
                               {{ $infos->links() }}
                           </nav>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>

   
@endsection

@section('js')
   <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
   <script>
       tinymce.init({
           selector: "textarea.content",
           
           plugins: [
               "advlist autolink lists link charmap print preview hr anchor pagebreak",
               "searchreplace wordcount visualblocks visualchars code",
               "insertdatetime media nonbreaking save table contextmenu directionality",
               "emoticons template paste textcolor colorpicker textpattern"
           ],
           toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
           toolbar2: "print preview | forecolor backcolor emoticons"
       });

   </script>
  
@endsection