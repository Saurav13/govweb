@extends('layouts.admin')

@section('body')
   
   <div class="content-header row">
   </div>

   <div class="content-body">
       <div class="card">
           <div class="card-header">
               <h4 class="card-title"><a data-action="collapse">Add New Personnel</a></h4>
               <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                       <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                   </ul>
               </div>
           </div>
           <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
               <div class="card-block card-dashboard">
                   <form class="form" method="POST" id="AddBlogForm" action="{{ route('personnels.store') }}" enctype="multipart/form-data">
                       {{ csrf_field() }}
                       <div class="form-body">
                            <h4 class="form-section"><i class="icon-eye6"></i> Personnel</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input class="form-control{{ $errors->has('en_name') ? ' border-danger' : '' }}" id="en_name" type="text" class="form-control" name="en_name" value="{{ old('en_name') }}" required>

                                        @if ($errors->has('en_name'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_name') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">नाम
                                        </label>
                                        <input class="form-control{{ $errors->has('ne_name') ? ' border-danger' : '' }}" id="ne_name" type="text" class="form-control" name="ne_name" value="{{ old('ne_name') }}">

                                        @if ($errors->has('ne_name'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_name') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                              
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Designation</label>
                                        @if ($errors->has('en_designation'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_designation') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('en_designation') ? ' border-danger' : '' }}" id="en_designation" type="text" class="form-control" name="en_designation" value="{{ old('en_designation') }}" required>
                                
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>पद</label>
                                        @if ($errors->has('ne_designation'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_designation') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('ne_designation') ? ' border-danger' : '' }}" id="ne_designation" type="text" class="form-control" name="ne_designation" value="{{ old('ne_designation') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Department</label>
                                        @if ($errors->has('en_department'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_department') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('en_department') ? ' border-danger' : '' }}" id="en_department" type="text" class="form-control" name="en_department" value="{{ old('en_department') }}" required>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>विभाग</label>
                                        @if ($errors->has('ne_department'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_department') }}</strong>
                                            </div>
                                        @endif
                                        <input class="form-control{{ $errors->has('ne_department') ? ' border-danger' : '' }}" id="ne_department" type="text" class="form-control" name="ne_department" value="{{ old('ne_department') }}">

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>About</label>
                                        @if ($errors->has('en_description'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('en_description') }}</strong>
                                            </div>
                                        @endif
                                        <textarea class="form-control{{ $errors->has('en_description') ? ' border-danger' : '' }}" id="en_description" type="text" class="form-control" name="en_description"  required>
                                                {{ old('en_description') }}
                                        </textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>केही सब्द</label>
                                        @if ($errors->has('ne_designation'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('ne_description') }}</strong>
                                            </div>
                                        @endif
                                        <textarea class="form-control{{ $errors->has('ne_description') ? ' border-danger' : '' }}" id="ne_description" type="text" class="form-control" name="ne_description" >
                                                {{ old('ne_description') }}
                                        </textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label>Image</label>
                                <input class="form-control{{ $errors->has('image') ? ' border-danger' : '' }}" type="file" placeholder="Photo"  name="image" required>

                                @if ($errors->has('image'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </div>
                                @endif
                            </div>



                       </div>

                       <div class="form-actions right">
                           <button type="submit" class="btn btn-primary">
                               <i class="icon-check2"></i> Save
                           </button>
                       </div>
                   </form>
               </div>
           </div>
       </div>

       <div class="card">
           <div class="card-header">
               <h4 class="card-title">Personnels</h4>
               <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                       <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                   </ul>
               </div>
           </div>
           <div class="card-body collapse in">
               <div class="card-block card-dashboard">
                   <div class="table-responsive">
                       <table class="table">
                           <thead>
                               <tr>
                                   <th>#</th>
                                   <th>Name</th>
                                   <th>Designation</th>
                                   <th>Department</th>
                                   <th>Image</th>
                                   <th width="20%">Actions</th>
                               </tr>
                           </thead>
                           <tbody>
                               @foreach($personnels as $personnel)
                               <tr>
                                   <td>{{ $loop->iteration + (($personnels->currentPage()-1) * $personnels->perPage()) }}</td>
                                   <td>{{ $personnel->en_name }}</td>
                                   <td>{{ $personnel->en_designation }}</td>
                                   <td>{{ $personnel->en_department }}</td>
                                   <td>
                                        <img class="img-fluid" src="{{ route('optimize', ['personnel_images',$personnel->image,100,100]) }}" alt="Card image cap">
                                   </td>
                                   <td>
                                        <a class="btn btn-outline-primary" title="View Details" href="{{ route('personnels.show',$personnel->id) }}"><i class="icon-eye"></i></a>

                                        <a class="btn btn-outline-warning" title="Update Details" href="{{ route('personnels.edit',$personnel->id) }}"><i class="icon-edit"></i></a>
                                        
                                        <form action="{{ route('personnels.destroy',$personnel->id) }}" method="POST" style="display:inline">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE" >
                                            <button id='deletePersonnel{{ $personnel->id }}' type="button" class="btn btn-outline-danger"><i class="icon-trash-o"></i></button>
                                        </form>
                                   </td>
                               </tr>
                               @endforeach
                           </tbody>
                       </table>


                       <div class="text-xs-center mb-3">
                           <nav aria-label="Page navigation">
                               {{ $personnels->links() }}
                           </nav>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>

   
@endsection