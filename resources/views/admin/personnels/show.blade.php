@extends('layouts.admin')

@section('body')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Personnel #{{ $personnel->id }}</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('personnels.index') }}">Back</a>
                    <a class="btn btn-warning btn-min-width mr-1 mb-1 " href="{{ route('personnels.edit',$personnel->id) }}">Edit</a>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section class="card">
            <div class="card-body collapse in">
                <div class="card-block">
                    
                    <div id="invoice-template" class="card-block">
                        
                        <div id="invoice-items-details" class="pt-2">
                            <div class="text-center" style="    text-align: center;">
                                <img class="img-fluid" src="{{ route('optimize', ['personnel_images',$personnel->image,150,150]) }}" alt="Card image cap">
                            </div>
                            <div class="col-md-12 col-sm-12 text-xs-center text-md-left">
                                <table class="table">
                                    <tbody style="border-top:0">
                                        <tr >
                                            <td  style="    border-top: 0;width:20%"><strong>Name: </strong></td>
                                            <td  style="    border-top: 0;">{{ $personnel->en_name }}</td>
                                        </tr>
                                        <tr >
                                            <td  style="    border-top: 0;"><strong>नाम: </strong></td>
                                            <td  style="    border-top: 0;">{{ $personnel->ne_name }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Designation: </strong></td>
                                            <td>{{ $personnel->en_designation }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>पद: </strong></td>
                                            <td>{{ $personnel->ne_designation }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Department: </strong></td>
                                            <td>{{ $personnel->en_department }}</td>
                                        </tr>
                                        
                                        <tr>
                                            <td><strong>विभाग: </strong></td>
                                            <td>{{ $personnel->ne_department }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>About: </strong></td>
                                            <td>{{ $personnel->en_description }}</td>
                                        </tr>
                                        
                                        <tr>
                                            <td><strong>केही सब्द: </strong></td>
                                            <td>{{ $personnel->ne_description }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
