<?php

return [

    'headers'=>[
        'l1'=> 'Government of Nepal',
        'l2'=>'Ministry of Energy, Water Resources and Irrigation',
        'l3'=>'Department of Water Resources and Irrigation',
        'l4'=>'Babai Irrigation Project',
        'l5'=>'Baidi, Bardiya'
    ],
    'nav' => [
        'home' => 'Home',
        'about' => 'About Us',
        'notices' => 'Notices',
        'news' => 'News',
        'projects' => 'Projects',
        'reports' => 'Reports',
        'gallery' => 'Gallery',
        'contact' => 'Contact Us',
        'citizenCharter'=>'Citizen Charter',
        'employees'=>'Working Employees'


    ],

    'general' => [
        'leave_a_message' => 'Leave A Message',
        'send' => 'Send',
        'contact_success' => "We'll respond to you soon.",
        'contact_error' => 'Please Provide All Necessary Information.',
    ],

    'contact' => [
        'l1' => 'Room No. 114 115',
        'l2' => 'Department of Water Resources and Irrigation buiding',
        'l3' => 'Jawalakhel, Patan, Lalitpur',
        'l4' => 'Phone: 01-5545730',
    ]
];