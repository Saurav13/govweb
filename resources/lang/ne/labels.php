<?php

return [
  
    'headers'=>[
        'l1'=> 'नेपाल सरकार',
        'l2'=>'उर्जा, जलस्रोत तथा सिंचाइ मन्त्तालय',
        'l3'=>'जलस्रोत तथा सिचाइ विभाग',
        'l4'=>'बबई सिंचाइ आयोजना',
        'l5'=>'वैदी, बर्दिया'
    ],

    'nav' => [
        'home' => 'गृहपृष्ठ',
        'about' => 'हाम्रो बारेमा',
        'notices' => 'सुचनाहरु',
        'news' => 'समाचार',
        'projects' => 'प्रोजेक्टहरु',
        'reports' => 'रिपोर्टहरु',
        'gallery' => 'ग्यालरी',
        'contact' => 'सम्पर्क',
        'citizenCharter'=>'नागरिक वडापत्र',
        'employees'=>'कार्यकर्ता'

    ],

    'general' => [
        'leave_a_message' => 'सम्पर्क गर्नुहोस्',
        'name' => 'नाम',
        'email' => 'इमेल',
        'send' => 'पठाउनुहोस्',
        'contact_success' => "We'll respond to you soon.",
        'contact_error' => 'Please Provide All Necessary Information.',
    ],

    'contact' => [
        'l1' => 'कोटाः ११४ र ११५',
        'l2' => 'जलस्रोत तथा सिंचाइ विभाग भवन',
        'l3' => 'जावलाखेल, पाटन, ललितपुर',
        'l4' => 'फोनः ०१-५५४५७३०',
    ]
];


