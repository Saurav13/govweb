<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Report;

class ReportController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $reports = Report::orderBy('updated_at','desc')->paginate(10);
        
        return view('admin.reports.index')->with('reports',$reports);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([          
            'contract_id' => 'required|max:191',
            'type1' => 'nullable|in:Annual,Quarterly,Monthly,Weekly',
            'type2' => 'nullable|in:Physical,Financial',
            // 'project_title' => 'required|max:191',
        ]);
        
        $report = new Report;

        $report->contract_id = $request->contract_id;
        // $report->project_title = $request->project_title;
        $report->details = $request->details;
        $report->estimated_amount = $request->estimated_amount;
        $report->contractor_name = $request->contractor_name;
        $report->contract_date = $request->contract_date;
        $report->contract_amount = $request->contract_amount;
        $report->due_date_completion = $request->due_date_completion;
        $report->bill_amount_till_date = $request->bill_amount_till_date;
        $report->payment_due = $request->payment_due;
        $report->financial_progress = $request->financial_progress;
        $report->attorney_holder_name = $request->attorney_holder_name;
        $report->attorney_holder_tel_no = $request->attorney_holder_tel_no;
        $report->mobilization_total = $request->mobilization_total;
        $report->mobilization_due = $request->mobilization_due;
        $report->PGB_amount = $request->PGB_amount;
        $report->PGB_date = $request->PGB_date;
        $report->APG_amount = $request->APG_amount;
        $report->APG_date = $request->APG_date;
        $report->remark_date = $request->remark_date;
        $report->side_sdk = $request->side_sdk;
        $report->type1 = $request->type1;
        $report->type2 = $request->type2;

        $report->save();

        $request->session()->flash('success', 'Report added.');        
        
        return redirect()->route('reports.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report = Report::findOrFail($id);

        return view('admin.reports.show',compact('report'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report = Report::findOrFail($id);

        return view('admin.reports.edit',compact('report'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([          
            'contract_id' => 'required|max:191',
            'type1' => 'nullable|in:Annual,Quarterly,Monthly,Weekly',
            'type2' => 'nullable|in:Physical,Financial',
            // 'project_title' => 'required|max:191',
        ]);

        $report = Report::findOrFail($id);        

        $report->contract_id = $request->contract_id;
        // $report->project_title = $request->project_title;
        $report->details = $request->details;
        $report->estimated_amount = $request->estimated_amount;
        $report->contractor_name = $request->contractor_name;
        $report->contract_date = $request->contract_date;
        $report->contract_amount = $request->contract_amount;
        $report->due_date_completion = $request->due_date_completion;
        $report->bill_amount_till_date = $request->bill_amount_till_date;
        $report->payment_due = $request->payment_due;
        $report->financial_progress = $request->financial_progress;
        $report->attorney_holder_name = $request->attorney_holder_name;
        $report->attorney_holder_tel_no = $request->attorney_holder_tel_no;
        $report->mobilization_total = $request->mobilization_total;
        $report->mobilization_due = $request->mobilization_due;
        $report->PGB_amount = $request->PGB_amount;
        $report->PGB_date = $request->PGB_date;
        $report->APG_amount = $request->APG_amount;
        $report->APG_date = $request->APG_date;
        $report->remark_date = $request->remark_date;
        $report->side_sdk = $request->side_sdk;
        $report->type1 = $request->type1;
        $report->type2 = $request->type2;

        $report->save();

        $request->session()->flash('success', 'Report updated.');        
        
        return redirect()->route('reports.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $report = Report::findOrFail($id);

        $report->delete();

        $request->session()->flash('success', 'Report deleted.');        
        
        return redirect()->route('reports.index');
    }
}
