<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notice;

class NoticeController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $notices = Notice::orderBy('updated_at','desc')->paginate(10);
        
        return view('admin.notices.index')->with('notices',$notices);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([         
            'en_title' => 'required|max:191',
            'type' => 'required|in:Tender,LOI,LOA,Others',
            'en_description' => 'required',
            'published_date' => 'required|date',
        ]);
        
        $notice = new Notice;
        $notice->en_title = $request->en_title;
        $notice->ne_title = $request->ne_title;
        $notice->en_description = $request->en_description;
        $notice->ne_description = $request->ne_description;
        $notice->type = $request->type;
        $notice->published_date = $request->published_date;

        $notice->save();

        $request->session()->flash('success', 'Notice added.');        
        
        return redirect()->route('notices.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notice = Notice::findOrFail($id);

        return view('admin.notices.show',compact('notice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notice = Notice::findOrFail($id);

        return view('admin.notices.edit',compact('notice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([          
            'en_title' => 'required|max:191',
            'en_description' => 'required',
            'type' => 'required|in:Tender,LOI,LOA,Others',
            'published_date' => 'required|date',
        ]);

        $notice = Notice::findOrFail($id);
        $notice->en_title = $request->en_title;
        $notice->ne_title = $request->ne_title;
        $notice->en_description = $request->en_description;
        $notice->ne_description = $request->ne_description;
        $notice->type = $request->type;
        $notice->published_date = $request->published_date;        

        $notice->save();

        $request->session()->flash('success', 'Notice updated.');        
        
        return redirect()->route('notices.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $notice = Notice::findOrFail($id);

        $notice->delete();

        $request->session()->flash('success', 'Notice deleted.');        
        
       return redirect()->back();
    }
}
