<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Personnel;

class PersonnelController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $personnels = Personnel::orderBy('updated_at','desc')->paginate(10);
        
        return view('admin.personnels.index')->with('personnels',$personnels);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([          
            'en_name' => 'required|max:191',
            'en_designation' => 'required|max:191',
            'en_department' => 'required|max:191',
            'image' => 'required|image'
        ]);

        $personnel = new Personnel;
        $personnel->en_name = $request->en_name;
        $personnel->ne_name = $request->ne_name;

        $personnel->en_designation = $request->en_designation;
        $personnel->ne_designation = $request->ne_designation;

        $personnel->en_department = $request->en_department;
        $personnel->ne_department = $request->ne_department;

        $personnel->en_description = $request->en_description;
        $personnel->ne_description = $request->ne_description;

        if($request->hasFile('image')){
            $photo = $request->file('image');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('personnel_images/');
            $photo->move($location,$filename);
            $personnel->image = $filename;
        }

        $personnel->save();

        $request->session()->flash('success', 'Personnel added.');        
        
        return redirect()->route('personnels.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $personnel = Personnel::findOrFail($id);

        return view('admin.personnels.show',compact('personnel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $personnel = Personnel::findOrFail($id);

        return view('admin.personnels.edit',compact('personnel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([          
            'en_name' => 'required|max:191',
            'en_designation' => 'required|max:191',
            'en_department' => 'required|max:191',
            'image' => 'nullable|image'
        ]);

        $personnel = Personnel::findOrFail($id);

        $personnel->en_name = $request->en_name;
        $personnel->ne_name = $request->ne_name;

        $personnel->en_designation = $request->en_designation;
        $personnel->ne_designation = $request->ne_designation;

        $personnel->en_description = $request->en_description;
        $personnel->ne_description = $request->ne_description;

        $personnel->en_department = $request->en_department;
        $personnel->ne_department = $request->ne_department;

        if($request->hasFile('image')){

            if($personnel->image)
                unlink(public_path('personnel_images/'.$personnel->image));

            $photo = $request->file('image');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('personnel_images/');
            $photo->move($location,$filename);
            $personnel->image = $filename;
        }

        $personnel->save();

        $request->session()->flash('success', 'Personnel updated.');        
        
        return redirect()->route('personnels.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $personnel = Personnel::findOrFail($id);

        if($personnel->image)
            unlink(public_path('personnel_images/'.$personnel->image));

        $personnel->delete();

        $request->session()->flash('success', 'Personnel deleted.');        
        
        return redirect()->route('personnels.index');
    }
}
