<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $news = News::orderBy('updated_at','desc')->paginate(10);
        
        return view('admin.news.index')->with('news',$news);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([          
            'en_title' => 'required|max:191',
            'published_date' => 'required|date',
            'en_description' => 'required',
            'image' => 'required|image'
        ]);

        $_news = new News;
        $_news->en_title = $request->en_title;
        $_news->ne_title = $request->ne_title;

        $_news->en_description = $request->en_description;
        $_news->ne_description = $request->ne_description;

        $_news->published_date = $request->published_date;

        if($request->hasFile('image')){
            $photo = $request->file('image');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('news_images/');
            $photo->move($location,$filename);
            $_news->image = $filename;
        }

        $_news->save();

        $request->session()->flash('success', 'News added.');        
        
        return redirect()->route('news.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $_news = News::findOrFail($id);

        return view('admin.news.show',compact('_news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $_news = News::findOrFail($id);

        return view('admin.news.edit',compact('_news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([          
            'en_title' => 'required|max:191',
            'en_description' => 'required',
            'published_date' => 'required|date',
            'image' => 'nullable|image'
        ]);

        $_news = News::findOrFail($id);
       
        $_news->en_title = $request->en_title;
        $_news->ne_title = $request->ne_title;

        $_news->en_description = $request->en_description;
        $_news->ne_description = $request->ne_description;

        $_news->published_date = $request->published_date;
        if($request->hasFile('image')){

            if($_news->image)
                unlink(public_path('news_images/'.$_news->image));

            $photo = $request->file('image');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('news_images/');
            $photo->move($location,$filename);
            $_news->image = $filename;
        }

        $_news->save();

        $request->session()->flash('success', 'News updated.');        
        
        return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $_news = News::findOrFail($id);

        if($_news->image)
            unlink(public_path('news_images/'.$_news->image));

        $_news->delete();

        $request->session()->flash('success', 'News deleted.');        
        
        return redirect()->route('news.index');
    }
}
