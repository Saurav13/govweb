<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;

class ProjectController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::orderBy('updated_at','desc')->paginate(10);
        
        return view('admin.projects.index')->with('projects',$projects);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([         
            'en_title' => 'required|max:191',
            'en_description' => 'required',
            'image' => 'required|image'
        ]);

        $project = new Project;
        $project->en_title = $request->en_title;
        $project->ne_title = $request->ne_title;
        $project->en_description = $request->en_description;
        $project->ne_description = $request->ne_description;

        if($request->hasFile('image')){
            $photo = $request->file('image');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('project_images/');
            $photo->move($location,$filename);
            $project->image = $filename;
        }

        $project->save();

        $request->session()->flash('success', 'Project added.');        
        
        return redirect()->route('projects.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::findOrFail($id);

        return view('admin.projects.show',compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::findOrFail($id);

        return view('admin.projects.edit',compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([          
            'en_title' => 'required|max:191',
            'en_description' => 'required',
            'image' => 'nullable|image'
        ]);

        $project = Project::findOrFail($id);
        $project->en_title = $request->en_title;
        $project->ne_title = $request->ne_title;
        $project->en_description = $request->en_description;
        $project->ne_description = $request->ne_description;


        $project->save();
        if($request->hasFile('image')){

            if($project->image)
                unlink(public_path('project_images/'.$project->image));

            $photo = $request->file('image');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('project_images/');
            $photo->move($location,$filename);
            $project->image = $filename;
        }

        $project->save();

        $request->session()->flash('success', 'Project updated.');        
        
        return redirect()->route('projects.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $project = Project::findOrFail($id);

        if($project->image)
            unlink(public_path('project_images/'.$project->image));

        $project->delete();

        $request->session()->flash('success', 'Project deleted.');        
        
       return redirect()->back();
    }
}
