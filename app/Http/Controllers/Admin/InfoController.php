<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Info;

class InfoController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $infos = Info::orderBy('updated_at','desc')->paginate(10);
        
        return view('admin.infos.index')->with('infos',$infos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([         
            'en_title' => 'required|max:191',
            'en_description' => 'required',
        ]);

        $info = new Info;
        $info->en_title = $request->en_title;
        $info->ne_title = $request->ne_title;
        $info->en_description = $request->en_description;
        $info->ne_description = $request->ne_description;

        $info->save();

        $request->session()->flash('success', 'Info added.');        
        
        return redirect()->route('infos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info = Info::findOrFail($id);

        return view('admin.infos.show',compact('info'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info = Info::findOrFail($id);

        return view('admin.infos.edit',compact('info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([          
            'en_title' => 'required|max:191',
            'en_description' => 'required',
        ]);

        $info = Info::findOrFail($id);
        $info->en_title = $request->en_title;
        $info->ne_title = $request->ne_title;
        $info->en_description = $request->en_description;
        $info->ne_description = $request->ne_description;

        $info->save();

        $request->session()->flash('success', 'Info updated.');        
        
        return redirect()->route('infos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $info = Info::findOrFail($id);
        $info->delete();

        $request->session()->flash('success', 'Info deleted.');        
        
       return redirect()->back();
    }
 
}
