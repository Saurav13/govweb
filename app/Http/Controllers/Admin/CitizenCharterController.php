<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CitizenCharter;

class CitizenCharterController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $citizencharters = CitizenCharter::orderBy('updated_at','desc')->paginate(10);
        
        return view('admin.citizencharters.index')->with('citizencharters',$citizencharters);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([          
            'en_service_title' => 'required|max:191',
            'en_service_fee' => 'required|max:191',
            'en_service_time' => 'required|max:191',
            'en_responsible_dept' => 'required|max:191',
            'en_remarks' => 'required',
        ]);

        $citizencharter = new CitizenCharter;
        $citizencharter->en_service_title = $request->en_service_title;
        $citizencharter->ne_service_title = $request->ne_service_title;

        $citizencharter->en_service_fee = $request->en_service_fee;
        $citizencharter->ne_service_fee = $request->ne_service_fee;

        $citizencharter->en_service_time = $request->en_service_time;
        $citizencharter->ne_service_time = $request->ne_service_time;

        $citizencharter->en_responsible_dept = $request->en_responsible_dept;
        $citizencharter->ne_responsible_dept = $request->ne_responsible_dept;

        $citizencharter->en_remarks = $request->en_remarks;
        $citizencharter->ne_remarks = $request->ne_remarks;

        $citizencharter->save();

        $request->session()->flash('success', 'CitizenCharter added.');        
        
        return redirect()->route('citizencharters.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $citizencharter = CitizenCharter::findOrFail($id);

        return view('admin.citizencharters.show',compact('citizencharter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $citizencharter = CitizenCharter::findOrFail($id);

        return view('admin.citizencharters.edit',compact('citizencharter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $citizencharter = CitizenCharter::findOrFail($id);

        $request->validate([          
            'en_service_title' => 'required|max:191',
            'en_service_fee' => 'required|max:191',
            'en_service_time' => 'required|max:191',
            'en_responsible_dept' => 'required|max:191',
            'en_remarks' => 'required',
        ]);

        $citizencharter->en_service_title = $request->en_service_title;
        $citizencharter->ne_service_title = $request->ne_service_title;

        $citizencharter->en_service_fee = $request->en_service_fee;
        $citizencharter->ne_service_fee = $request->ne_service_fee;

        $citizencharter->en_service_time = $request->en_service_time;
        $citizencharter->ne_service_time = $request->ne_service_time;

        $citizencharter->en_responsible_dept = $request->en_responsible_dept;
        $citizencharter->ne_responsible_dept = $request->ne_responsible_dept;

        $citizencharter->en_remarks = $request->en_remarks;
        $citizencharter->ne_remarks = $request->ne_remarks;

        $citizencharter->save();

        $request->session()->flash('success', 'CitizenCharter updated.');        
        
        return redirect()->route('citizencharters.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $citizencharter = CitizenCharter::findOrFail($id);

        $citizencharter->delete();

        $request->session()->flash('success', 'CitizenCharter deleted.');        
        
        return redirect()->route('citizencharters.index');
    }
}
