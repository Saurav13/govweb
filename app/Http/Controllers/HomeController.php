<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;
use App\Gallery;
use App\Info;
use App\News;
use App\Notice;
use App\Report;
use App\Project;
use App\Personnel;
use App\ContactUsMessage;
use App\CitizenCharter;
use Validator;

class HomeController extends Controller
{
   
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $newsList = News::orderBy('published_date','desc')->limit(6)->get();
        $notices=Notice::orderBy('published_date','desc')->limit(5)->get();
        $galleries=Gallery::limit(9)->get();
        $personnels=Personnel::all();

        return view('frontend.home',compact('newsList','notices','personnels','galleries'));
    }

    public function notices(){
        $notices = Notice::orderBy('published_date','desc')->paginate(10);
        return view('frontend.notice',compact('notices'));
    }

    public function news(){
        $newsList = News::orderBy('published_date','desc')->paginate(10);
        return view('frontend.news',compact('newsList'));
    }
    public function citizenCharter(){
        $charters = CitizenCharter::all();
        return view('frontend.citizen_charter',compact('charters'));
    }

    public function singleNews($slug){
        $news = News::where('slug',$slug)->firstOrFail();

        return view('frontend.singlenews',compact('news'));
    }

    public function projects(){
        $projects = Project::orderBy('id','desc')->paginate(10);
        return view('frontend.projects',compact('projects'));
    }

    public function singleProject($slug){
        $project = Project::where('slug',$slug)->firstOrFail();

        return view('frontend.singleproject',compact('project'));
    }

    public function reports(){
        $reports = Report::orderBy('id','desc')->get();
        return view('frontend.reports',compact('reports'));
    }

    public function filterReports(Request $request){
        $reports = Report::where('type1',$request->type1)->where('type2',$request->type2)->orderBy('id','desc')->get();
        return view('frontend.reports',compact('reports'));
    }
    

    public function printReport(){
        $reports = Report::orderBy('id','desc')->get();
        return view('frontend.printReport',compact('reports'));
    }

    public function albums()
    {
        $albums=Album::orderBy('created_at','desc')->paginate(6);
        return view('frontend.albums')->with('albums',$albums);
        
    }
    public function albumImages($slug)
    {
        $album=Album::where('slug',$slug)->firstOrFail();
        $images=$album->album_images()->paginate(9);
       
        return view('frontend.album_images')->with('album',$album)->with('images',$images);
    }

    public function info($alias){
        $info = Info::where('slug',$alias)->firstOrFail();

        return view('frontend.info',compact('info'));
    }

    public function contact_us(Request $request){
        return view('frontend.contact');
    }

    public function contactSend(Request $request){
        $validator = Validator::make($request->all(), [
            'name'=>'required|max:191',
            'email'=> 'required|email',
            'message'=> 'required',
        ]);

        if ($validator->fails()) {
            $request->session()->flash('contact_error', 'Please Provide All Necessary Information.');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $contact_message = new ContactUsMessage;
        $contact_message->name = $request->name;
        $contact_message->email = $request->email;
        $contact_message->subject = $request->subject;
        $contact_message->message = $request->message;
        $contact_message->save();
        
        $request->session()->flash('contact_success', "We'll respond to you soon.");
        return redirect()->route('contact');        
    }
}
