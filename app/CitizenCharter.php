<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CitizenCharter extends Model
{
    public function getServiceTitleAttribute(){
        $locale = app()->getLocale();

        return $this->{$locale.'_service_title'} ? : $this->en_service_title;
    }

    public function getServiceFeeAttribute(){
        $locale = app()->getLocale();

        return $this->{$locale.'_service_fee'} ? : $this->en_service_fee;
    }

    public function getServiceTimeAttribute(){
        $locale = app()->getLocale();

        return $this->{$locale.'_service_time'} ? : $this->en_service_time;
    }

    public function getResponsibleDeptAttribute(){
        $locale = app()->getLocale();

        return $this->{$locale.'_responsible_dept'} ? : $this->en_responsible_dept;
    }

    public function getRemarksAttribute(){
        $locale = app()->getLocale();

        return $this->{$locale.'_remarks'} ? : $this->en_remarks;
    }
}
