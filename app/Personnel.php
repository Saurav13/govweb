<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personnel extends Model
{
    
    public function getNameAttribute(){
        $locale = app()->getLocale();

        return $this->{$locale.'_name'} ? : $this->en_name;
    }
    public function getDepartmentAttribute(){
        $locale = app()->getLocale();

        return $this->{$locale.'_department'} ? : $this->en_department;
    }
    public function getDesignationAttribute(){
        $locale = app()->getLocale();

        return $this->{$locale.'_designation'} ? : $this->en_designation;
    }

    public function getDescriptionAttribute(){
        $locale = app()->getLocale();

        return $this->{$locale.'_description'} ? : $this->en_description;
    }
}
