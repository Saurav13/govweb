<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Info extends Model
{
    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'en_title'
            ]
        ];
    }

    public function getTitleAttribute(){
        $locale = app()->getLocale();

        return $this->{$locale.'_title'} ? : $this->en_title;
    }

    public function getDescriptionAttribute(){
        $locale = app()->getLocale();

        return $this->{$locale.'_description'} ? : $this->en_description;
    }
}
