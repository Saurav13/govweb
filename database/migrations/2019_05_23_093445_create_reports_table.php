<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('slug')->unique();

            $table->string('contract_id');
            $table->text('details')->nullable();
            $table->string('estimated_amount')->nullable();
            $table->string('contractor_name')->nullable();
            $table->string('contract_date')->nullable();
            $table->string('contract_amount')->nullable();
            $table->string('due_date_completion')->nullable();
            $table->string('bill_amount_till_date')->nullable();
            $table->string('payment_due')->nullable();
            $table->string('financial_progress')->nullable();
            $table->string('attorney_holder_name')->nullable();
            $table->string('attorney_holder_tel_no')->nullable();
            $table->string('mobilization_total')->nullable();
            $table->string('mobilization_due')->nullable();
            $table->string('PGB_amount')->nullable();
            $table->string('PGB_date')->nullable();
            $table->string('APG_amount')->nullable();
            $table->string('APG_date')->nullable();
            $table->string('remark_date')->nullable();
            $table->string('side_sdk')->nullable();
            $table->timestamps();
        });
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
