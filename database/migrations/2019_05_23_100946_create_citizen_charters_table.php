<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitizenChartersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citizen_charters', function (Blueprint $table) {
            $table->increments('id');


            $table->string('en_service_title')->nullable();
            $table->string('ne_service_title')->nullable();

            $table->string('en_service_fee')->nullable();
            $table->string('ne_service_fee')->nullable();

            $table->string('en_service_time')->nullable();
            $table->string('ne_service_time')->nullable();

            $table->string('en_responsible_dept')->nullable();
            $table->string('ne_responsible_dept')->nullable();

            $table->text('en_remarks')->nullable();
            $table->text('ne_remarks')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citizen_charters');
    }
}
